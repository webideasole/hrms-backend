USE [master]
GO
/****** Object:  Database [cf_hrms]    Script Date: 04-01-2019 8:34:15 PM ******/
CREATE DATABASE [cf_hrms]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'cf_hrms', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\cf_hrms.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'cf_hrms_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\cf_hrms_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [cf_hrms] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [cf_hrms].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [cf_hrms] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [cf_hrms] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [cf_hrms] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [cf_hrms] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [cf_hrms] SET ARITHABORT OFF 
GO
ALTER DATABASE [cf_hrms] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [cf_hrms] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [cf_hrms] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [cf_hrms] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [cf_hrms] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [cf_hrms] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [cf_hrms] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [cf_hrms] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [cf_hrms] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [cf_hrms] SET  DISABLE_BROKER 
GO
ALTER DATABASE [cf_hrms] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [cf_hrms] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [cf_hrms] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [cf_hrms] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [cf_hrms] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [cf_hrms] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [cf_hrms] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [cf_hrms] SET RECOVERY FULL 
GO
ALTER DATABASE [cf_hrms] SET  MULTI_USER 
GO
ALTER DATABASE [cf_hrms] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [cf_hrms] SET DB_CHAINING OFF 
GO
ALTER DATABASE [cf_hrms] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [cf_hrms] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [cf_hrms] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'cf_hrms', N'ON'
GO
ALTER DATABASE [cf_hrms] SET QUERY_STORE = OFF
GO
USE [cf_hrms]
GO
/****** Object:  User [root]    Script Date: 04-01-2019 8:34:16 PM ******/
CREATE USER [root] FOR LOGIN [root] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [root]
GO
ALTER ROLE [db_accessadmin] ADD MEMBER [root]
GO
ALTER ROLE [db_securityadmin] ADD MEMBER [root]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [root]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [root]
GO
ALTER ROLE [db_datareader] ADD MEMBER [root]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [root]
GO
ALTER ROLE [db_denydatareader] ADD MEMBER [root]
GO
ALTER ROLE [db_denydatawriter] ADD MEMBER [root]
GO
/****** Object:  Table [dbo].[branch_master]    Script Date: 04-01-2019 8:34:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[branch_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_branch_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bussiness_unit_master]    Script Date: 04-01-2019 8:34:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bussiness_unit_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_bussiness_unit_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configuration_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[configuration_master](
	[id] [int] NOT NULL,
	[title] [varchar](200) NULL,
	[parent_id] [int] NULL,
	[order] [int] NULL,
	[is_last_level] [bit] NULL,
	[steps_count] [int] NULL,
	[is_finished] [bit] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
 CONSTRAINT [PK_configuration_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[configuration_progress]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[configuration_progress](
	[id] [int] NOT NULL,
	[configuration_master_id] [int] NULL,
	[title] [varchar](200) NULL,
	[step] [int] NULL,
	[is_last_level] [bit] NULL,
	[is_finished] [bit] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
 CONSTRAINT [PK_configuration_progress] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[cost_center_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cost_center_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_cost_center_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[country_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[country_master](
	[id] [char](10) NOT NULL,
	[name] [nvarchar](100) NULL,
	[nationality] [nvarchar](100) NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[custom_group_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[custom_group_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_custom_group_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[department_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[department_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_department_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[division_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[division_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_division_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employee_type_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employee_type_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_employee_type_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[employment_type_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employment_type_master](
	[id] [int] NOT NULL,
 CONSTRAINT [PK_employment_type_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[job_title_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[job_title_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_job_title_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[leave_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[leave_master](
	[id] [int] NOT NULL,
	[code] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[type] [varchar](50) NULL,
	[level] [varchar](50) NULL,
	[color_code] [varchar](50) NULL,
	[leave_unit] [tinyint] NULL,
 CONSTRAINT [PK_leave_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[level_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[level_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_level_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[location_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[location_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_location_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ocupation_group_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ocupation_group_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_ocupation_group_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[paycode_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paycode_master](
	[id] [int] NOT NULL,
 CONSTRAINT [PK_paycode_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[period_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[period_master](
	[id] [int] NOT NULL,
	[year] [date] NULL,
	[pay_frequency] [varchar](200) NULL,
	[title] [varchar](400) NULL,
	[date_from] [date] NULL,
	[date_to] [date] NULL,
	[working_days] [varchar](400) NULL,
	[total_hours] [real] NULL,
 CONSTRAINT [PK_period_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[section_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[section_master](
	[id] [int] NOT NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_section_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[shift_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[shift_master](
	[id] [int] NOT NULL,
 CONSTRAINT [PK_shift_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[subsection_master]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[subsection_master](
	[id] [int] NOT NULL,
	[section_master_id] [int] NULL,
	[name] [varchar](400) NULL,
 CONSTRAINT [PK_subsection_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_insurance_details]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_insurance_details](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[pf_id] [nchar](10) NULL,
	[pf_effective_date] [nchar](10) NULL,
	[spouce_tax_status] [nchar](10) NULL,
	[child_count] [nchar](10) NULL,
	[ytd_sso] [nchar](10) NULL,
	[ytd_tax] [nchar](10) NULL,
	[ytd_taxable] [nchar](10) NULL,
	[prev_salary] [nchar](10) NULL,
	[prev_ytd_tax] [nchar](10) NULL,
	[prev_sso] [nchar](10) NULL,
	[income_tax_method] [nchar](10) NULL,
 CONSTRAINT [PK_user_insurance_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_leave_allocation]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_leave_allocation](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[leave_master_id] [int] NULL,
 CONSTRAINT [PK_user_leave_allocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_leave_application]    Script Date: 04-01-2019 8:34:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_leave_application](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[leave_master_id] [int] NULL,
	[reason] [text] NULL,
	[from] [datetime] NULL,
	[to] [datetime] NULL,
	[approval_date] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
	[substitute_id] [int] NULL,
 CONSTRAINT [PK_user_leave_request] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_leave_application_permission]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_leave_application_permission](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[approver_1] [int] NULL,
	[approver_2] [int] NULL,
	[approver_3] [int] NULL,
	[notification_reciepent_1] [int] NULL,
	[notification_reciepent_2] [int] NULL,
	[notification_reciepent_3] [int] NULL,
 CONSTRAINT [PK_user_leave_application_permission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_master]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_master](
	[id] [int] NOT NULL,
	[user_roles_id] [int] NULL,
	[company_code] [nchar](10) NULL,
	[username] [varchar](400) NULL,
	[email] [varchar](400) NULL,
	[password] [varchar](400) NULL,
	[forgot_password_token] [varchar](400) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
 CONSTRAINT [PK_user_master] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_notification]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_notification](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[active_text] [text] NULL,
	[passive] [text] NULL,
	[linked_user_id] [text] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
 CONSTRAINT [PK_user_notification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_overtime_application]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_overtime_application](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
 CONSTRAINT [PK_user_overtime_application] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_paycode_allocation]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_paycode_allocation](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[paycode_master_id] [int] NULL,
 CONSTRAINT [PK_user_paycode_allocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_personal_details]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_personal_details](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[employee_code] [varchar](50) NULL,
	[image_location] [varchar](400) NULL,
	[prefix] [nchar](10) NULL,
	[first_name_en] [varchar](400) NULL,
	[first_name_th] [varchar](400) NULL,
	[middle_name_en] [varchar](400) NULL,
	[middle_name_th] [varchar](400) NULL,
	[last_name_en] [varchar](400) NULL,
	[last_name_th] [varchar](400) NULL,
	[address_en] [text] NOT NULL,
	[address_th] [text] NULL,
	[tel_no] [varchar](15) NULL,
	[mobile_no] [varchar](15) NULL,
	[personal_email] [varchar](400) NULL,
	[gender] [tinyint] NULL,
	[dob] [date] NULL,
	[marital_status] [bit] NULL,
	[nationality] [varchar](100) NULL,
	[religion] [varchar](100) NULL,
	[blood_group] [varchar](10) NULL,
	[personal_id_no] [varchar](100) NULL,
	[id_expiry_date] [date] NULL,
	[passport_no] [varchar](100) NULL,
	[passport_expiry_date] [date] NULL,
	[ssn] [varchar](100) NULL,
	[bank_code] [varchar](50) NULL,
	[bank_ac_no] [varchar](100) NULL,
	[tax_no] [varchar](100) NULL,
 CONSTRAINT [PK_user_personal_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_proffessional_details]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_proffessional_details](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[recruitment_source] [varchar](200) NULL,
	[work_status] [varchar](200) NULL,
	[job_title_id] [int] NULL,
	[employment_type_id] [int] NULL,
	[employee_type_id] [int] NULL,
	[official_email] [varchar](400) NULL,
	[joining_date] [date] NULL,
	[probation_pass_date] [date] NULL,
	[employment_date] [date] NULL,
	[branch_id] [int] NULL,
	[division_id] [int] NULL,
	[department_id] [int] NULL,
	[section_id] [int] NULL,
	[sub_section_id] [int] NULL,
	[location_id] [int] NULL,
	[level_id] [int] NULL,
	[occupation_group_id] [int] NULL,
	[bussiness_unit_id] [int] NULL,
	[cost_center_id] [int] NULL,
	[custom_group_id] [int] NULL,
 CONSTRAINT [PK_user_proffessional_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_roles]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_roles](
	[id] [int] NOT NULL,
	[name] [varchar](50) NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
	[status] [tinyint] NULL,
 CONSTRAINT [PK_user_roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_salary_details]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_salary_details](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[basic_salary] [float] NULL,
	[self_expense] [float] NULL,
	[self_exemption] [float] NULL,
	[spouce_with_income_allowance] [float] NULL,
	[spouce_without_income_allowance] [float] NULL,
	[children_allowance] [float] NULL,
	[parents_allowance] [float] NULL,
	[parents_insurance_premium] [float] NULL,
	[life_insurance_premium] [float] NULL,
	[disable_personos_charity] [float] NULL,
	[house_loan_interest] [float] NULL,
	[provident_fund] [float] NULL,
	[donation] [float] NULL,
	[rmf] [float] NULL,
	[ltf] [float] NULL,
	[domestic_expense_exemption] [float] NULL,
	[serverance] [float] NULL,
	[social_security_contribution] [float] NULL,
	[age_allowance] [float] NULL,
	[al1] [float] NULL,
	[al2] [float] NULL,
	[al3] [float] NULL,
	[children_education_allowance] [float] NULL,
	[education_support_donation] [float] NULL,
	[house_loan_insurance] [float] NULL,
 CONSTRAINT [PK_user_salary_details] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_shift_allocation]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_shift_allocation](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[shift_master_id] [int] NULL,
 CONSTRAINT [PK_user_shift_allocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_shift_application_permission]    Script Date: 04-01-2019 8:34:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_shift_application_permission](
	[id] [int] NOT NULL,
	[user_master_id] [int] NULL,
	[approver_1] [int] NULL,
	[approver_2] [int] NULL,
	[approver_3] [int] NULL,
	[notification_reciepent_1] [int] NULL,
	[notification_reciepent_2] [int] NULL,
	[notification_reciepent_3] [int] NULL,
 CONSTRAINT [PK_user_shift_application_permission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[configuration_master]  WITH CHECK ADD  CONSTRAINT [FK_configuration_master_configuration_master] FOREIGN KEY([parent_id])
REFERENCES [dbo].[configuration_master] ([id])
GO
ALTER TABLE [dbo].[configuration_master] CHECK CONSTRAINT [FK_configuration_master_configuration_master]
GO
ALTER TABLE [dbo].[configuration_progress]  WITH CHECK ADD  CONSTRAINT [FK_configuration_progress_configuration_master1] FOREIGN KEY([configuration_master_id])
REFERENCES [dbo].[configuration_master] ([id])
GO
ALTER TABLE [dbo].[configuration_progress] CHECK CONSTRAINT [FK_configuration_progress_configuration_master1]
GO
ALTER TABLE [dbo].[subsection_master]  WITH CHECK ADD  CONSTRAINT [FK_subsection_master_section_master] FOREIGN KEY([section_master_id])
REFERENCES [dbo].[section_master] ([id])
GO
ALTER TABLE [dbo].[subsection_master] CHECK CONSTRAINT [FK_subsection_master_section_master]
GO
ALTER TABLE [dbo].[user_insurance_details]  WITH CHECK ADD  CONSTRAINT [FK_user_insurance_details_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_insurance_details] CHECK CONSTRAINT [FK_user_insurance_details_user_master]
GO
ALTER TABLE [dbo].[user_leave_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_allocation_leave_master] FOREIGN KEY([leave_master_id])
REFERENCES [dbo].[leave_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_allocation] CHECK CONSTRAINT [FK_user_leave_allocation_leave_master]
GO
ALTER TABLE [dbo].[user_leave_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_allocation_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_allocation] CHECK CONSTRAINT [FK_user_leave_allocation_user_master]
GO
ALTER TABLE [dbo].[user_leave_application]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_request_leave_master] FOREIGN KEY([leave_master_id])
REFERENCES [dbo].[leave_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application] CHECK CONSTRAINT [FK_user_leave_request_leave_master]
GO
ALTER TABLE [dbo].[user_leave_application]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_request_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application] CHECK CONSTRAINT [FK_user_leave_request_user_master]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master1] FOREIGN KEY([approver_1])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master1]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master2] FOREIGN KEY([approver_2])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master2]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master3] FOREIGN KEY([approver_3])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master3]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master4] FOREIGN KEY([notification_reciepent_1])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master4]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master5] FOREIGN KEY([notification_reciepent_2])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master5]
GO
ALTER TABLE [dbo].[user_leave_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_leave_application_permission_user_master6] FOREIGN KEY([notification_reciepent_3])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_leave_application_permission] CHECK CONSTRAINT [FK_user_leave_application_permission_user_master6]
GO
ALTER TABLE [dbo].[user_master]  WITH CHECK ADD  CONSTRAINT [FK_user_master_user_roles] FOREIGN KEY([user_roles_id])
REFERENCES [dbo].[user_roles] ([id])
GO
ALTER TABLE [dbo].[user_master] CHECK CONSTRAINT [FK_user_master_user_roles]
GO
ALTER TABLE [dbo].[user_notification]  WITH CHECK ADD  CONSTRAINT [FK_user_notification_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_notification] CHECK CONSTRAINT [FK_user_notification_user_master]
GO
ALTER TABLE [dbo].[user_overtime_application]  WITH CHECK ADD  CONSTRAINT [FK_user_overtime_application_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_overtime_application] CHECK CONSTRAINT [FK_user_overtime_application_user_master]
GO
ALTER TABLE [dbo].[user_paycode_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_paycode_allocation_paycode_master] FOREIGN KEY([paycode_master_id])
REFERENCES [dbo].[paycode_master] ([id])
GO
ALTER TABLE [dbo].[user_paycode_allocation] CHECK CONSTRAINT [FK_user_paycode_allocation_paycode_master]
GO
ALTER TABLE [dbo].[user_paycode_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_paycode_allocation_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_paycode_allocation] CHECK CONSTRAINT [FK_user_paycode_allocation_user_master]
GO
ALTER TABLE [dbo].[user_personal_details]  WITH CHECK ADD  CONSTRAINT [FK_user_personal_details_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_personal_details] CHECK CONSTRAINT [FK_user_personal_details_user_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_branch_master] FOREIGN KEY([branch_id])
REFERENCES [dbo].[branch_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_branch_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_bussiness_unit_master] FOREIGN KEY([bussiness_unit_id])
REFERENCES [dbo].[bussiness_unit_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_bussiness_unit_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_cost_center_id] FOREIGN KEY([cost_center_id])
REFERENCES [dbo].[cost_center_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_cost_center_id]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_custom_group_master] FOREIGN KEY([custom_group_id])
REFERENCES [dbo].[custom_group_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_custom_group_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_department_master] FOREIGN KEY([department_id])
REFERENCES [dbo].[department_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_department_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_division_master] FOREIGN KEY([division_id])
REFERENCES [dbo].[division_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_division_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_employee_type_master] FOREIGN KEY([employee_type_id])
REFERENCES [dbo].[employee_type_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_employee_type_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_employment_type_master] FOREIGN KEY([employment_type_id])
REFERENCES [dbo].[employment_type_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_employment_type_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_job_title_master] FOREIGN KEY([job_title_id])
REFERENCES [dbo].[job_title_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_job_title_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_level_master] FOREIGN KEY([level_id])
REFERENCES [dbo].[level_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_level_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_location_master] FOREIGN KEY([location_id])
REFERENCES [dbo].[location_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_location_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_ocupation_group_master] FOREIGN KEY([occupation_group_id])
REFERENCES [dbo].[ocupation_group_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_ocupation_group_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_section_master] FOREIGN KEY([section_id])
REFERENCES [dbo].[section_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_section_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_subsection_master] FOREIGN KEY([sub_section_id])
REFERENCES [dbo].[subsection_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_subsection_master]
GO
ALTER TABLE [dbo].[user_proffessional_details]  WITH CHECK ADD  CONSTRAINT [FK_user_proffessional_details_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_proffessional_details] CHECK CONSTRAINT [FK_user_proffessional_details_user_master]
GO
ALTER TABLE [dbo].[user_salary_details]  WITH CHECK ADD  CONSTRAINT [FK_user_salary_details_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_salary_details] CHECK CONSTRAINT [FK_user_salary_details_user_master]
GO
ALTER TABLE [dbo].[user_shift_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_allocation_shift_master] FOREIGN KEY([shift_master_id])
REFERENCES [dbo].[shift_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_allocation] CHECK CONSTRAINT [FK_user_shift_allocation_shift_master]
GO
ALTER TABLE [dbo].[user_shift_allocation]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_allocation_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_allocation] CHECK CONSTRAINT [FK_user_shift_allocation_user_master]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master] FOREIGN KEY([user_master_id])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master1] FOREIGN KEY([approver_1])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master1]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master2] FOREIGN KEY([approver_2])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master2]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master3] FOREIGN KEY([approver_3])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master3]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master4] FOREIGN KEY([notification_reciepent_1])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master4]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master5] FOREIGN KEY([notification_reciepent_2])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master5]
GO
ALTER TABLE [dbo].[user_shift_application_permission]  WITH CHECK ADD  CONSTRAINT [FK_user_shift_application_permission_user_master6] FOREIGN KEY([notification_reciepent_3])
REFERENCES [dbo].[user_master] ([id])
GO
ALTER TABLE [dbo].[user_shift_application_permission] CHECK CONSTRAINT [FK_user_shift_application_permission_user_master6]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0=>female,1=>male' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'user_personal_details', @level2type=N'COLUMN',@level2name=N'gender'
GO
USE [master]
GO
ALTER DATABASE [cf_hrms] SET  READ_WRITE 
GO
