const Sequelize = require('sequelize');
var sequelize = require('../db');
var UserMaster = require('../models/users');
const admin_permission_details = sequelize.define('AdminPermissionDetails', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    payroll:{
        type: Sequelize.TINYINT,
        defaultValue: 0
    },
    leave:{
        type: Sequelize.TINYINT,
        defaultValue: 0
    },
    time:{
        type: Sequelize.TINYINT,
        defaultValue: 0
    }
}, {
    timestamps: false,
    tableName: 'admin_permission_details'
})

// admin_permission_details.hasOne(UserMster, { foreignKey: 'user_master_id', targetKey: 'id' })
// admin_permission_details.hasMany(UserMaster, { foreignKey: 'user_master_id' });

module.exports = admin_permission_details