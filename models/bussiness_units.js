const Sequelize = require('sequelize');
var sequelize = require('../db');

const bussiness_unit = sequelize.define('BussinessUnitsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'bussiness_unit_master'
});

module.exports = bussiness_unit;