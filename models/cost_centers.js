const Sequelize = require('sequelize');
var sequelize = require('../db');

const cost_centers = sequelize.define('CostCentersMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'cost_center_master'
});

module.exports = cost_centers;