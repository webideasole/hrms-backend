const Sequelize = require('sequelize');
var sequelize = require('../db');

const custom_groups = sequelize.define('CustomGroupsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'custom_group_master'
});

module.exports = custom_groups;