const Sequelize = require('sequelize');
var sequelize = require('../db');

const departments = sequelize.define('DepartmentsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'department_master'
});

module.exports = departments;