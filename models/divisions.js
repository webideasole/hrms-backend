const Sequelize = require('sequelize');
var sequelize = require('../db');

const divisions = sequelize.define('DivisionsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'division_master'
});

module.exports = divisions;