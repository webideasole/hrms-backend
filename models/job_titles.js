const Sequelize = require('sequelize');
var sequelize = require('../db');

const job_titles = sequelize.define('JobTitlesMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'job_title_master'
});

module.exports = job_titles;