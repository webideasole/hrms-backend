const Sequelize = require('sequelize');
var sequelize = require('../db');

const leaves = sequelize.define('LeavesMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    code: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    },
    type: {
        type: Sequelize.STRING
    },
    level: {
        type: Sequelize.STRING
    },
    color_code: {
        type: Sequelize.STRING
    },
    leave_unit: {
        type: Sequelize.STRING
    },
    count: {
        type: Sequelize.INTEGER,
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'leave_master'
});

module.exports = leaves;