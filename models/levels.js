const Sequelize = require('sequelize');
var sequelize = require('../db');

const levels = sequelize.define('LevelsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'level_master'
});

module.exports = levels;