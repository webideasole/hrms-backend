const Sequelize = require('sequelize');
var sequelize = require('../db');

const locations = sequelize.define('LocationsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'location_master'
});

module.exports = locations;