const Sequelize = require('sequelize');
var sequelize = require('../db');

const occupation_groups = sequelize.define('OccupationGroupsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'ocupation_group_master'
});

module.exports = occupation_groups;