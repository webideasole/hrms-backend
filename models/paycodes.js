const Sequelize = require('sequelize');
var sequelize = require('../db');

const paycodes = sequelize.define('PaycodesMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    code: {
        type: Sequelize.STRING
    },
    is_fixed: {
        type: Sequelize.BOOLEAN
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'paycode_master'
});

module.exports = paycodes;