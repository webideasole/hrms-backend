const Sequelize = require('sequelize');
var sequelize = require('../db');

const salary_payment_type_master = sequelize.define('SalaryPaymentTypeMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        code: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'salary_payment_type_master'
});

module.exports = salary_payment_type_master;