const Sequelize = require('sequelize');
var sequelize = require('../db');

const sections = sequelize.define('SectionsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'section_master'
});

module.exports = sections;