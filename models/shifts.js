const Sequelize = require('sequelize');
var sequelize = require('../db');

const shifts = sequelize.define('ShiftsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING
    },
    code: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'shift_master'
});

module.exports = shifts;