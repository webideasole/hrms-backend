const Sequelize = require('sequelize');
var sequelize = require('../db');

const subsections = sequelize.define('SubSectionsMaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    section_master_id :{
        type: Sequelize.INTEGER,
    },
    name: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'subsection_master'
});

module.exports = subsections;