const Sequelize = require("sequelize");
var dB = require("../db");

const UserMaster = require("./users");

const Task = dB.define(
  "task",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    user_master_id: {
      type: Sequelize.INTEGER
    },
    approver_id: {
      type: Sequelize.INTEGER,
      defaultValue: null
    },
    task: {
      type: Sequelize.STRING
    },
    approval_date: {
      type: Sequelize.DATE,
      defaultValue: null
    },
    approval_remarks: {
      type: Sequelize.STRING,
      defaultValue: null
    },
    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      type: Sequelize.DATE
    },
    status: {
      type: Sequelize.TINYINT,
      defaultValue: 0
    }
  },
  {
    timestamps: false,
    freezeTableName: true,
    tableName: "user_task"
  }
);

Task.belongsTo(UserMaster, {
  foreignKey: "user_master_id",
  as: "UserMaster"
});
Task.belongsTo(UserMaster, { foreignKey: "approver_id" });

module.exports = Task;