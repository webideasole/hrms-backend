const Sequelize = require('sequelize');
var sequelize = require('../db');

const user_attendance_details = sequelize.define('UserAttendanceDetails', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    checkin_time: {
        type: Sequelize.DATE,
        allowNull: true
    },
    checkin_long: {
        type: Sequelize.FLOAT,
        allowNull: true
    },
    checkin_lat: {
        type: Sequelize.FLOAT,
        allowNull: true
    },
    checkin_location: {
        type: Sequelize.STRING,
        allowNull: true
    },
    checkin_pin: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    checkout_time: {
        type: Sequelize.DATE,
        allowNull: true
    },
    checkout_long: {
        type: Sequelize.FLOAT,
        allowNull: true
    },
    checkout_lat: {
        type: Sequelize.FLOAT,
        allowNull: true
    },
    checkout_location: {
        type: Sequelize.STRING,
        allowNull: true
    },
    checkout_pin: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    created_at: {
        type: Sequelize.DATE,
        allowNull: true
    },
    updated_at: {
        type: Sequelize.DATE,
        allowNull: true
    },
    is_regularised: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    regulariser_id : {
        type: Sequelize.INTEGER,
        defaultValue: null
    },
    status: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
    
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_attendance_details'
});
module.exports = user_attendance_details;