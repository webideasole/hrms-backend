const Sequelize = require('sequelize');
var sequelize = require('../db');
const UserMaster=require('./users');

const user_insurance_detail = sequelize.define('InsuranceDetail', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    pf_id: {
        type: Sequelize.STRING
    },
    pf_effective_date: {
        type: Sequelize.DATE
    },
    spouce_tax_status: {
        type: Sequelize.STRING
    },
    child_count: {
        type: Sequelize.INTEGER
    },
    ytd_sso: {
        type: Sequelize.STRING
    },
    ytd_tax: {
        type: Sequelize.STRING
    },
    ytd_taxable: {
        type: Sequelize.STRING
    },
    prev_salary: {
        type: Sequelize.STRING
    },
    prev_ytd_tax: {
        type: Sequelize.STRING
    },
    prev_sso: {
        type: Sequelize.STRING
    },
    income_tax_method: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_insurance_details'
});
//user_insurance_detail.hasOne(UserMaster, {foreignKey: 'user_master_id', targetKey: 'id'});
module.exports = user_insurance_detail;