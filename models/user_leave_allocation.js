const Sequelize = require('sequelize');
var sequelize = require('../db');
const LeaveMaster=require('./leaves');

const user_leave_allocation = sequelize.define('LeaveAllocations', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    leave_master_id: {
        type: Sequelize.INTEGER
    },
    carry_forward: {
        type: Sequelize.FLOAT
    },
    entitlement: {
        type: Sequelize.FLOAT
    },
    ytd_availed: {
        type: Sequelize.FLOAT
    },
    balance: {
        type: Sequelize.FLOAT
    }
    
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_leave_allocation'
});
user_leave_allocation.belongsTo(LeaveMaster, {foreignKey: 'leave_master_id'});
module.exports = user_leave_allocation;