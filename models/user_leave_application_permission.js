const Sequelize = require('sequelize');
var sequelize = require('../db');

var PersonalDetails = require('./user_personal_details');
var ProffessionalDetails = require('./user_proffessional_details');

const user_leave_application_permission = sequelize.define('UserLeaveApplicationPermission', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    approver_level: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_leave_application_permission'
});
user_leave_application_permission.belongsTo(PersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'UserPersonalDetail'});
user_leave_application_permission.belongsTo(PersonalDetails, { foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetail'});
user_leave_application_permission.belongsTo(ProffessionalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id',as:'UserProffessionalDetail'});
user_leave_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'approver_id', targetKey: 'user_master_id',as:'ApproverProffessionalDetail'});
module.exports = user_leave_application_permission;