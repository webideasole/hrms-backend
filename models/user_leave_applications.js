const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');
var UserLeaveAllocationDetails = require('./user_leave_allocation');
var UserLeaveApplicationPermissions = require('./user_leave_application_permission');

const user_leave_application = sequelize.define('UserLeaveApplication', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    user_leave_allocation_id: {
        type: Sequelize.INTEGER
    },
    substitute_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    attachment: {
        type: Sequelize.TEXT
    },
    from: {
        type: Sequelize.DATE
    },
    to: {
        type: Sequelize.DATE
    },
    leave_unit_count: {
        type: Sequelize.INTEGER
    },
    approval_date: {
        type: Sequelize.DATE
    },
    approval_remarks: {
        type: Sequelize.TEXT
    },
    reason: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    },
    step: {
        type: Sequelize.INTEGER
    },
    statusText:{
        type:Sequelize.VIRTUAL
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_leave_application'
});
user_leave_application.belongsTo(UserMaster, {foreignKey: 'user_master_id'});
user_leave_application.hasMany(UserLeaveApplicationPermissions, {foreignKey: 'user_master_id', sourceKey: 'user_master_id', as: 'UserLeaveApplicationPermission'});
// user_leave_application.hasOne(UserLeaveApplicationPermissions, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'UserLeaveApplicationPermissionCurrentApprover'});
user_leave_application.belongsTo(UserLeaveAllocationDetails, {foreignKey: 'user_leave_allocation_id'});
user_leave_application.belongsTo(UserPersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'ApplicantPersonalDetails'});
user_leave_application.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
user_leave_application.belongsTo(UserPersonalDetails, {foreignKey: 'substitute_id', targetKey: 'user_master_id', as: 'SubstitutePersonalDetails'});
module.exports = user_leave_application;