const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');
var UserLeaveApplications = require('./user_leave_applications');

const user_leave_approver = sequelize.define('UserLeaveApprover', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    leave_application_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    approver_level: {
        type: Sequelize.INTEGER
    },
    created_at: {
        type: Sequelize.DATE
    },
    approval_status: {
        type: Sequelize.INTEGER
    },
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_leave_approver'
});
user_leave_approver.belongsTo(UserMaster, {foreignKey: 'approver_id'});
user_leave_approver.belongsTo(UserLeaveApplications, {foreignKey: 'leave_application_id'});
user_leave_approver.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_leave_approver;