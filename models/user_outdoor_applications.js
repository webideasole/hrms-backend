const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');

const user_outdoor_application = sequelize.define('UserOutdoorApplication', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    attachment: {
        type: Sequelize.TEXT
    },
    start_date: {
        type: Sequelize.DATE
    },
    end_date: {
        type: Sequelize.DATE
    },
    date_count: {
        type: Sequelize.INTEGER
    },
    start_time: {
        type: Sequelize.TEXT
    },
    end_time: {
        type: Sequelize.STRING
    },
    approval_date: {
        type: Sequelize.DATE
    },
    approval_remarks: {
        type: Sequelize.TEXT
    },
    reason: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_outdoor_application'
});
user_outdoor_application.belongsTo(UserMaster, {foreignKey: 'user_master_id'});
user_outdoor_application.belongsTo(UserPersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'ApplicantPersonalDetails'});
user_outdoor_application.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_outdoor_application;