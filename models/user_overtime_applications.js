const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');

const user_overtime_application = sequelize.define('UserOvertimeApplication', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    attachment: {
        type: Sequelize.TEXT
    },
    date: {
        type: Sequelize.DATE
    },
    from_time: {
        type: Sequelize.TEXT
    },
    to_time: {
        type: Sequelize.STRING
    },
    approval_date: {
        type: Sequelize.DATE
    },
    approval_remarks: {
        type: Sequelize.TEXT
    },
    reason: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_overtime_application'
});
user_overtime_application.belongsTo(UserMaster, {foreignKey: 'user_master_id'});
user_overtime_application.belongsTo(UserPersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'ApplicantPersonalDetails'});
user_overtime_application.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_overtime_application;