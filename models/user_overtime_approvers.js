const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');
var UserOvertimeApplications = require('./user_overtime_applications');

const user_overtime_approver = sequelize.define('UserOvertimeApprover', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    overtime_application_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    approver_level: {
        type: Sequelize.INTEGER
    },
    created_at: {
        type: Sequelize.DATE
    },
    approval_status: {
        type: Sequelize.INTEGER
    },
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_overtime_approver'
});
user_overtime_approver.belongsTo(UserMaster, {foreignKey: 'approver_id'});
user_overtime_approver.belongsTo(UserOvertimeApplications, {foreignKey: 'overtime_application_id'});
user_overtime_approver.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_overtime_approver;