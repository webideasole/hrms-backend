const Sequelize = require('sequelize');
var sequelize = require('../db');
const PaycodeMaster=require('./paycodes');

const user_paycode_allocation = sequelize.define('PaycodeAllocations', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    paycode_master_id: {
        type: Sequelize.INTEGER
    },
    user_paycode_value: {
        type: Sequelize.FLOAT
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_paycode_allocation'
});
user_paycode_allocation.belongsTo(PaycodeMaster, {foreignKey: 'paycode_master_id'});
module.exports = user_paycode_allocation;