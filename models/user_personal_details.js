const Sequelize = require('sequelize');
var sequelize = require('../db');
//const UserMaster=require('./users');
const CountryMaster = require('./countries');

const user_personal_detail = sequelize.define('PersonalDetails', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    employee_code: {
        type: Sequelize.STRING
    },
    image_location: {
        type: Sequelize.STRING
    },
    prefix: {
        type: Sequelize.STRING
    },
    first_name_en: {
        type: Sequelize.STRING
    },
    first_name_th: {
        type: Sequelize.STRING
    },
    middle_name_en: {
        type: Sequelize.STRING
    },
    middle_name_th: {
        type: Sequelize.STRING
    },
    last_name_en: {
        type: Sequelize.STRING
    },
    last_name_th: {
        type: Sequelize.STRING
    },
    address_en: {
        type: Sequelize.TEXT
    },
    address_th: {
        type: Sequelize.TEXT
    },
    tel_no: {
        type: Sequelize.STRING
    },
    mobile_no: {
        type: Sequelize.STRING
    },
    personal_email: {
        type: Sequelize.STRING
    },
    gender: {
        type: Sequelize.STRING
    },
    dob: {
        type: Sequelize.DATE
    },
    marital_status: {
        type: Sequelize.INTEGER
    },
    nationality: {
        type: Sequelize.STRING
    },
    religion: {
        type: Sequelize.STRING
    },
    blood_group: {
        type: Sequelize.STRING
    },
    personal_id_no: {
        type: Sequelize.STRING
    },
    id_expiry_date: {
        type: Sequelize.DATE
    },
    passport_no: {
        type: Sequelize.STRING
    },
    passport_expiry_date: {
        type: Sequelize.DATE
    },
    ssn: {
        type: Sequelize.STRING
    },
    bank_code: {
        type: Sequelize.STRING
    },
    bank_ac_no: {
        type: Sequelize.STRING
    },
    tax_no: {
        type: Sequelize.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_personal_details'
});
user_personal_detail.belongsTo(CountryMaster, {foreignKey: 'nationality', targetKey: 'id'});
module.exports = user_personal_detail;