const Sequelize = require('sequelize');
const sequelize = require('../db');
const JobTitlesMaster = require('./job_titles');
const BranchesMaster = require('./branches');
const DepartmentsMaster = require('./department');
const SectionsMaster = require('./sections');
const SubSectionsMaster = require('./sub_sections');
const LocationsMaster = require('./locations');
const DivisionsMaster = require('./divisions');
const LevelsMaster = require('./levels');
const OccupationGroupsMaster = require('./occupation_groups');
const BussinessUnitsMaster = require('./bussiness_units');
const CostCentersMaster = require('./cost_centers');
const CustomGroupsMaster = require('./custom_groups');
const user_proffessional_detail = sequelize.define('ProffessionalDetails', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    recruitment_source: {
        type: Sequelize.STRING
    },
    work_status: {
        type: Sequelize.STRING
    },
    employment_type_id: {
        type: Sequelize.STRING
    },
    employee_type_id: {
        type: Sequelize.STRING
    },
    official_email: {
        type: Sequelize.STRING
    },
    joining_date: {
        type: Sequelize.DATE
    },
    probation_pass_date: {
        type: Sequelize.DATE
    },
    employment_date: {
        type: Sequelize.DATE
    },
    manager_id: {
        type: Sequelize.INTEGER
    },
    branch_id: {
        type: Sequelize.INTEGER
    },
    division_id: {
        type: Sequelize.INTEGER
    },
    department_id: {
        type: Sequelize.INTEGER
    },
    section_id: {
        type: Sequelize.INTEGER
    },
    sub_section_id: {
        type: Sequelize.INTEGER
    },
    location_id: {
        type: Sequelize.INTEGER
    },
    level_id: {
        type: Sequelize.INTEGER
    },
    occupation_group_id: {
        type: Sequelize.INTEGER
    },
    bussiness_unit_id: {
        type: Sequelize.INTEGER
    },
    cost_center_id: {
        type: Sequelize.INTEGER
    },
    custom_group_id: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_proffessional_details'
});
user_proffessional_detail.belongsTo(JobTitlesMaster, {foreignKey: 'job_title_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(BranchesMaster, {foreignKey: 'branch_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(DivisionsMaster, {foreignKey: 'division_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(DepartmentsMaster, {foreignKey: 'department_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(SectionsMaster, {foreignKey: 'section_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(SubSectionsMaster, {foreignKey: 'sub_section_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(LevelsMaster, {foreignKey: 'level_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(LocationsMaster, {foreignKey: 'location_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(OccupationGroupsMaster, {foreignKey: 'occupation_group_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(BussinessUnitsMaster, {foreignKey: 'bussiness_unit_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(CostCentersMaster, {foreignKey: 'cost_center_id', targetKey: 'id'});
user_proffessional_detail.belongsTo(CustomGroupsMaster, {foreignKey: 'custom_group_id', targetKey: 'id'});
module.exports = user_proffessional_detail;