const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');

const user_reimbursement_application = sequelize.define('UserReimbursementApplication', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    claim_type: {
        type: Sequelize.STRING
    },
    start_date: {
        type: Sequelize.DATE
    },
    end_date: {
        type: Sequelize.DATE
    },
    date_count: {
        type: Sequelize.INTEGER
    },
    start_time: {
        type: Sequelize.TEXT
    },
    end_time: {
        type: Sequelize.STRING
    },
    amount: {
        type: Sequelize.FLOAT
    },
    reason: {
        type: Sequelize.TEXT
    },
    attachment: {
        type: Sequelize.TEXT
    },
    approval_date: {
        type: Sequelize.DATE
    },
    approval_remarks: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_reimbursement_application'
});
user_reimbursement_application.belongsTo(UserMaster, {foreignKey: 'user_master_id'});
user_reimbursement_application.belongsTo(UserPersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'ApplicantPersonalDetails'});
user_reimbursement_application.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_reimbursement_application;