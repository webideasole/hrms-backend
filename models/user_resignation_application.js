const Sequelize = require('sequelize');
var sequelize = require('../db');

const UserResignationApplication = sequelize.define('UserResignationApplication', {
    id : {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id :{ 
        type: Sequelize.INTEGER,
    },
    reason : {
        type: Sequelize.STRING
    },
    date : {
        type: Sequelize.DATE
    },
    notice_period : {
        type: Sequelize.INTEGER,
    },
    last_working_day : {
        type: Sequelize.DATE,
    },
    attachment : {
        type: Sequelize.STRING,
        defaultValue: null
    },
    approver_id : {
        type: Sequelize.INTEGER,
        defaultValue: null
    },
    remarks : {
        type: Sequelize.STRING,
        defaultValue: null
    },
    approval_date : {
        type: Sequelize.DATE
    },
    status : {
        type: Sequelize.TINYINT,
        defaultValue : 0
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    }
}, {
        timestamps: false,

        freezeTableName: true,
        tableName: 'user_resignation_application'
    })

module.exports = UserResignationApplication;