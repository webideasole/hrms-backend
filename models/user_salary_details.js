const Sequelize = require('sequelize');
var sequelize = require('../db');
const UserMaster=require('./users');

const user_salary_detail = sequelize.define('SalaryDetail', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    basic_salary: {
        type: Sequelize.FLOAT
    },
    payment_type_id: {
        type: Sequelize.INTEGER
    },
    self_expense: {
        type: Sequelize.FLOAT
    },
    self_exemption: {
        type: Sequelize.STRING
    },
    spouce_type: {
        type: Sequelize.INTEGER
    },
    spouce_type_income: {
        type: Sequelize.FLOAT
    },
    children_allowance: {
        type: Sequelize.FLOAT
    },
    parents_allowance: {
        type: Sequelize.FLOAT
    },
    parents_insurance_premium: {
        type: Sequelize.FLOAT
    },
    life_insurance_premium: {
        type: Sequelize.FLOAT
    },
    disable_personos_charity: {
        type: Sequelize.FLOAT
    },
    house_loan_interest: {
        type: Sequelize.FLOAT
    },
    provident_fund: {
        type: Sequelize.FLOAT
    },
    donation: {
        type: Sequelize.FLOAT
    },
    rmf: {
        type: Sequelize.FLOAT
    },
    ltf: {
        type: Sequelize.FLOAT
    },
    domestic_expense_exemption: {
        type: Sequelize.FLOAT
    },
    serverance: {
        type: Sequelize.FLOAT
    },
    social_security_contribution: {
        type: Sequelize.FLOAT
    },
    age_allowance: {
        type: Sequelize.FLOAT
    },
    al1: {
        type: Sequelize.FLOAT
    },
    al2: {
        type: Sequelize.FLOAT
    },
    al3: {
        type: Sequelize.FLOAT
    },
    children_education_allowance: {
        type: Sequelize.FLOAT
    },
    education_support_donation: {
        type: Sequelize.FLOAT
    },
    house_loan_insurance: {
        type: Sequelize.FLOAT
    },
    children_other_allowance: {
        type: Sequelize.FLOAT
    },
    donation_other_allowance: {
        type: Sequelize.FLOAT
    },
    life_insurance_other_allowance: {
        type: Sequelize.FLOAT
    },
    spouce_insurance_other_allowance: {
        type: Sequelize.FLOAT
    },
    parents_insurance_other_allowance: {
        type: Sequelize.FLOAT
    },
    personal_other_allowance: {
        type: Sequelize.FLOAT
    },
    pf_other_allowance: {
        type: Sequelize.FLOAT
    },
    sport_support_donation_other_allowance: {
        type: Sequelize.FLOAT
    },
    social_security_other_allowance: {
        type: Sequelize.FLOAT
    },
    spouce_other_allowance: {
        type: Sequelize.FLOAT
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_salary_details'
});
//user_proffessional_detail.hasOne(UserMaster, {foreignKey: 'user_master_id', targetKey: 'id'});
module.exports = user_salary_detail;