const Sequelize = require('sequelize');
var sequelize = require('../db');
const ShiftMaster = require('./shifts');

const user_shift_allocation = sequelize.define('ShiftAllocations', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    shift_master_id: {
        type: Sequelize.INTEGER
    },
    start_date: {
        type: Sequelize.DATE
    },
    end_date: {
        type: Sequelize.DATE
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_shift_allocation'
});
user_shift_allocation.belongsTo(ShiftMaster, {foreignKey: 'shift_master_id'});
module.exports = user_shift_allocation;