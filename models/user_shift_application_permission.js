const Sequelize = require('sequelize');
var sequelize = require('../db');

var PersonalDetails = require('./user_personal_details');
var ProffessionalDetails = require('./user_proffessional_details');

const user_shift_application_permission = sequelize.define('UserShiftApplicationPermission', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    shift_approver_1: {
        type: Sequelize.INTEGER
    },
    shift_approver_2: {
        type: Sequelize.INTEGER
    },
    shift_approver_3: {
        type: Sequelize.INTEGER
    },
    shift_notification_1: {
        type: Sequelize.INTEGER
    },
    shift_notification_2: {
        type: Sequelize.INTEGER
    },
    shift_notification_3: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_shift_application_permission'
});
user_shift_application_permission.belongsTo(PersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'UserPersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_approver_1', targetKey: 'user_master_id', as: 'Approver1PersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_approver_2', targetKey: 'user_master_id', as: 'Approver2PersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_approver_3', targetKey: 'user_master_id', as: 'Approver3PersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_notification_1', targetKey: 'user_master_id', as: 'Notifier1PersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_notification_2', targetKey: 'user_master_id', as: 'Notifier2PersonalDetail'});
user_shift_application_permission.belongsTo(PersonalDetails, { foreignKey: 'shift_notification_3', targetKey: 'user_master_id', as: 'Notifier3PersonalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id',as:'UserProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'shift_approver_1', targetKey: 'user_master_id',as:'Approver1ProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, {foreignKey: 'shift_approver_2', targetKey: 'user_master_id',as:'Approver2ProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'shift_approver_3', targetKey: 'user_master_id',as:'Approver3ProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'shift_notification_1', targetKey: 'user_master_id',as:'Notifier1ProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'shift_notification_2', targetKey: 'user_master_id',as:'Notifier2ProffessionalDetail'});
user_shift_application_permission.belongsTo(ProffessionalDetails, { foreignKey: 'shift_notification_3', targetKey: 'user_master_id',as:'Notifier3ProffessionalDetail'});
module.exports = user_shift_application_permission;