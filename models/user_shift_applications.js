const Sequelize = require('sequelize');
var sequelize = require('../db');

var UserMaster = require('./users');
var UserPersonalDetails = require('./user_personal_details');
var UserShiftMasterDetails = require('./shifts');

const user_shift_application = sequelize.define('UserShiftApplication', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_master_id: {
        type: Sequelize.INTEGER
    },
    approver_id: {
        type: Sequelize.INTEGER
    },
    user_shift_allocation_id: {
        type: Sequelize.STRING
    },
    start_date: {
        type: Sequelize.DATE
    },
    end_date: {
        type: Sequelize.DATE
    },
    date_count: {
        type: Sequelize.INTEGER
    },
    old_shift_id: {
        type: Sequelize.INTEGER
    },
    new_shift_id: {
        type: Sequelize.INTEGER
    },
    reason: {
        type: Sequelize.TEXT
    },
    attachment: {
        type: Sequelize.TEXT
    },
    approval_date: {
        type: Sequelize.DATE
    },
    approval_remarks: {
        type: Sequelize.TEXT
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: 'user_shift_application'
});
user_shift_application.belongsTo(UserMaster, {foreignKey: 'user_master_id'});
user_shift_application.belongsTo(UserShiftMasterDetails, {foreignKey: 'old_shift_id', targetKey: 'id', as: 'OldShiftDetails'});
user_shift_application.belongsTo(UserShiftMasterDetails, {foreignKey: 'new_shift_id', targetKey: 'id', as: 'NewShiftDetails'});
user_shift_application.belongsTo(UserPersonalDetails, {foreignKey: 'user_master_id', targetKey: 'user_master_id', as: 'ApplicantPersonalDetails'});
user_shift_application.belongsTo(UserPersonalDetails, {foreignKey: 'approver_id', targetKey: 'user_master_id', as: 'ApproverPersonalDetails'});
module.exports = user_shift_application;