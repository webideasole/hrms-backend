const Sequelize = require('sequelize');
var sequelize = require('../db');
var bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync(10);
;

var PersonalDetails = require('./user_personal_details');
var ProffessionalDetails = require('./user_proffessional_details');
var InsuranceDetails = require('./user_insurance_details');
var SalaryDetails = require('./user_salary_details');
var paycodeAllocationDetails = require('./user_paycode_allocation');
var ShiftAllocationDetails = require('./user_shift_allocation');
var LeaveAllocationDetails = require('./user_leave_allocation');
var UserLeaveApplicationPermissionModel = require('./user_leave_application_permission');
var UserShiftApplicationPermissionModel = require('./user_shift_application_permission');
var AdminPermissionDetails=require('./admin_permission_details');

const users = sequelize.define('usermaster', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_roles_id: {
        type: Sequelize.INTEGER
    },
    company_code: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        defaultValue: null
    },
    username: {
        type: Sequelize.STRING,
        unique: true
    },
    password: {
        type: Sequelize.STRING(400)
    },
    forgot_password_token: {
        type: Sequelize.STRING(400)
    },
    created_at: {
        type: Sequelize.DATE
    },
    updated_at: {
        type: Sequelize.DATE
    },
    status: {
        type: Sequelize.INTEGER
    },
    hash: {
        type: Sequelize.VIRTUAL
//        get () {
//      return this.getDataValue('firstName') + ' ' + this.getDataValue('lastName')
//    }
    }
}, {
    timestamps: false,
//    createdAt: 'created_at',
//    updatedAt: 'updated_at',
    freezeTableName: true,
    tableName: 'user_master'
});

users.prototype.setPassword = async function (password) {
    var result;
    result = bcrypt.hashSync(password, salt);
    return result;

};

users.prototype.validPassword = async function (password) {
    // var result;
    return new Promise((resolve, reject) => {
        let check = bcrypt.compare(password, this.getDataValue('password'));
        if (check) {
            resolve(check)
        } else {
            reject(check)
        }
    })

    // console.log("step1" + result);
    // return result;

    // return result;
};

users.beforeCreate(function (user, options) {
    return new Promise((resolve, reject) => {
        user.setPassword(user.getDataValue('password')).then((hash) => {
            user.setDataValue('password', hash);
            console.log(user.getDataValue('password'));
            if (user.getDataValue('password').length < 5) {
                throw new error('Password creation error');
            }
        });
        return resolve(user, options);
    });
});
users.hasOne(PersonalDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasOne(ProffessionalDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasOne(InsuranceDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasOne(SalaryDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasMany(paycodeAllocationDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasMany(UserLeaveApplicationPermissionModel, {foreignKey: 'user_master_id', targetKey: 'id', as: 'UserLeaveApplicationPermission'});
users.hasOne(UserShiftApplicationPermissionModel, {foreignKey: 'user_master_id', targetKey: 'id', as: 'UserShiftApplicationPermission'});
users.hasOne(AdminPermissionDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasMany(ShiftAllocationDetails, {foreignKey: 'user_master_id', targetKey: 'id'});
users.hasMany(LeaveAllocationDetails, {foreignKey: 'user_master_id', targetKey: 'id'});

module.exports = users;