var JwtStrategy = require('passport-jwt').Strategy,
        ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
var User = require('./models/users');
const salt = require('./parameters').passwordHash;

module.exports = function (passport) {
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
    opts.secretOrKey = salt;
    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
        User.findOne({where: {username: jwt_payload.username}}).then(user => {
            done(null, user);
        }, error => {
            done(null, false);

        });
    }));
};
