const express = require("express"),
    router = express.Router();
const mime = require("mime");
const base64 = require("file-base64");
var passport = require('passport');
require('../passport')(passport);
var jwt = require('jsonwebtoken');
const salt = require('../parameters').passwordHash;
const UserModel = require('../models/users');
const UserProffessionalDetailsModel = require("../models/user_proffessional_details");
const UserPersonalDetailsModel = require("../models/user_personal_details");
const UserAttendanceDetailsModel = require("../models/user_attendance_details");
const Sequelize = require("sequelize");
const sequelize = require('../db');
const Op = Sequelize.Op;
router.post("/check_in", passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserAttendanceDetailsModel.findOne({
            where: {
                user_master_id: req.body.id,
                checkout_time: null
            },
            order: [
                ['id', 'DESC']
            ],
            limit: 1,
            attributes: ['checkout_time']
        }).then((details) => {

            if (details && !details.checkout_time) {
                res.json({
                    code: res.statusCode,
                    status: 'ERROR',
                    success: false,
                    msg: 'Please Regularise your attendance before proceeding'
                });
            } else {
                UserAttendanceDetailsModel.create({
                    user_master_id: req.body.id,
                    checkin_time: req.body.timestamp,
                    checkin_long: req.body.long,
                    checkin_lat: req.body.lat,
                    checkin_location: req.body.city,
                    checkin_pin: req.body.zip,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0
                }).then((data) => {
                    res.json({
                        code: res.statusCode,
                        status: 'Success',
                        success: true,
                        msg: 'Successfully Chaecked in.'
                    });
                });
            }
        });
    }
});
router.post("/check_out", passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserAttendanceDetailsModel.findOne({
            where: {
                user_master_id: req.body.id,
                checkout_time: null
            },
            order: [
                ['id', 'DESC']
            ],
            limit: 1,
            attributes: ['id', 'checkout_time']
        }).then((details) => {

            if (details && !details.checkout_time) {
                UserAttendanceDetailsModel.update({
                    user_master_id: req.body.id,
                    checkout_time: req.body.timestamp,
                    checkout_long: req.body.long,
                    checkout_lat: req.body.lat,
                    checkout_location: req.body.city,
                    checkout_pin: req.body.zip,
                    updated_at: new Date(Date.now()).toISOString(),
                    status: 1
                },
                    {
                        where: {
                            id: details.id
                        }

                    }
                ).then((data) => {
                    res.json({
                        code: res.statusCode,
                        status: 'Success',
                        success: true,
                        msg: 'Successfully Chaecked Out.'
                    });
                });
            } else {
                res.json({
                    code: res.statusCode,
                    status: 'ERROR',
                    success: false,
                    msg: 'Please Regularise your attendance before proceeding'
                });
            }
        });
    }
});
router.post("/check_attendance_status", passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserAttendanceDetailsModel.findOne({
            where: {
                user_master_id: req.body.id,
                checkout_time: null
            },
            order: [
                ['id', 'DESC']
            ],
            limit: 1,
            attributes: ['id', 'checkout_time', 'checkin_time', 'checkin_location']
        }).then((details) => {
            if (details && !details.checkout_time) {
                res.json({
                    code: res.statusCode,
                    status: 'Success',
                    success: true,
                    check_in: true,
                    check_in_time: details.checkin_time,
                    check_in_city: details.checkin_location
                });
            } else {
                res.json({
                    code: res.statusCode,
                    status: 'Succeess',
                    success: true,
                    check_in: false,
                    check_in_time: null,
                    check_in_city: null
                });
            }
        });
    }
});
router.post("/get_monthly_attendance", passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        sequelize.query(`SELECT min_id, max_id, date_now, uad1.checkin_time, uad2.checkout_time 
FROM (SELECT MIN(id) as min_id,MAX(id) as max_id, CONVERT(date, created_at) as date_now FROM user_attendance_details  
 WHERE MONTH(created_at)=${req.body.month + 1} AND YEAR(created_at)=${req.body.year} AND user_master_id=${req.body.id} GROUP BY CONVERT(date, created_at)) as uad0 
INNER JOIN user_attendance_details uad1 ON uad0.min_id=uad1.id
INNER JOIN user_attendance_details uad2 ON uad0.max_id=uad2.id`, { model: UserModel }).then((attndnce_details) => {
            var FirstDay = new Date(req.body.year, req.body.month, 01);
            var LastDay = new Date(req.body.year, req.body.month + 1, 01);
            console.log("first", FirstDay);
            console.log("last", LastDay);




            sequelize.query(`SELECT [from], [to] from user_leave_application WHERE user_master_id =116 AND [from] 
< '${LastDay.toISOString()}' AND [to] > '${FirstDay.toISOString()}' AND status=1`, { model: UserModel }).then((leave_dtls) => {
                // console.log(attndnce_details);
                // console.log(leave_dtls);
                // if (attndnce_details.length > 0 && leave_dtls.length > 0) {
                //     let data = Array.concat(
                //       attndnce_details,
                //       leave_dtls
                //     );
                // }


                res.json({
                    code: 200,
                    success: true,
                    data: attndnce_details.concat(leave_dtls)
                });
            });
        });
    }
});

router.post("/last-checkintime", function (req, res, next) {
    sequelize.query(`SELECT min_id, max_id, date_now, uad1.checkin_time, uad2.checkout_time 
    FROM (SELECT MIN(id) as min_id,MAX(id) as max_id, CONVERT(date, created_at) as date_now FROM user_attendance_details  
     WHERE Convert(date,created_at)='${(new Date(req.body.date)).toISOString().slice(0,10)}' AND user_master_id=${req.body.emp_id} GROUP BY CONVERT(date, created_at)) as uad0 
    INNER JOIN user_attendance_details uad1 ON uad0.min_id=uad1.id
    INNER JOIN user_attendance_details uad2 ON uad0.max_id=uad2.id`, { model: UserModel }).then((entryies) => {
        res.json({
            code: res.statusCode,
            success: true,
            entryies
        })
    });
})


router.post("/create_regularization", passport.authenticate('jwt', { session: false }), function (req, res, next) {

    const { id, emp_id, type, date, from, to } = req.body;

    if (type == 1 || type == 3 || type == 2) {
        let newDate = new Date(date)
        console.log(newDate);

        sequelize.query(`SELECT *, CONVERT(date, checkin_time) AS [datetime]  FROM user_attendance_details where CONVERT(date, checkin_time)  = '${newDate.toISOString()}' and user_master_id = '${emp_id}'`, { model: UserAttendanceDetailsModel }).then(attdence => {
            if (from) {

                var checkindate = new Date(`${date} ${from} UTC`);
            }
            if (to) {

                var checkoutdate = new Date(`${date} ${to} UTC`);
            }
            console.log(checkoutdate);

            if (attdence.length > 0) {
                UserAttendanceDetailsModel.update({
                    checkout_time: checkoutdate ? checkoutdate.toISOString() : null,
                    checkout_long: req.body.long,
                    checkout_lat: req.body.lat,
                    checkout_location: req.body.city,
                    checkout_pin: req.body.zip,

                    checkin_time: checkindate ? checkindate.toISOString() : null,
                    checkin_long: req.body.long,
                    checkin_lat: req.body.lat,
                    checkin_location: req.body.city,
                    checkin_pin: req.body.zip,
                    is_regularised: true,
                    regulariser_id: id,
                    status: 2,
                    updated_at: new Date(Date.now()).toISOString(),
                }, {
                        where: {
                            id: attdence[0].id
                        }

                    }).then(updated => {
                        if (updated) {

                            res.json({
                                code: res.statusCode,
                                success: true,
                                msg: "Updated successfully"
                            })
                        }
                    })

            } else {
                UserAttendanceDetailsModel.create({
                    checkout_time: checkoutdate ? checkoutdate.toISOString() : null,
                    checkout_long: req.body.long,
                    checkout_lat: req.body.lat,
                    checkout_location: req.body.city,
                    checkout_pin: req.body.zip,
                    user_master_id: emp_id,
                    checkin_time: checkindate ? checkindate.toISOString() : null,
                    checkin_long: req.body.long,
                    checkin_lat: req.body.lat,
                    checkin_location: req.body.city,
                    checkin_pin: req.body.zip,
                    is_regularised: true,
                    regulariser_id: id,
                    status: 2,
                    created_at: new Date(Date.now()).toISOString(),
                }).then(userdata => {
                    res.json({
                        code: res.statusCode,
                        success: true,
                        userdata
                    })
                })
            }
            // console.log(data);

        });
    } else {
        console.log("Eleseee");

    }
});



saveFileToDiskAndGetFilename = function (request_file, path) {
    if (request_file !== null) {
        return new Promise(function (resolve, reject) {

            let base64File = request_file.split(';base64,').pop();
            let fileType = request_file.split(';base64,')[0];
            fileType = fileType.substr(5);
            let ext = mime.getExtension(fileType);
            base64.decode(base64File, path + '.' + ext, function (err, output) {
                if (!err) {
                    // console.log(output);
                    resolve(output);
                } else {
                    reject(err);
                }
            });
        })
    }

}

isUserUnderMySupervision = (my_id, target_user_id) => {
    console.log("my_id: ", my_id, " target_user_id: ", target_user_id);
    return new Promise(function (resolve, reject) {
        UserModel.findOne(
            {
                where: {
                    id: my_id
                }
            }).then(({ user_roles_id }) => {
                if (user_roles_id <= 2) {
                    resolve(true);
                } else {
                    UserProffessionalDetailsModel.findOne({
                        where: {
                            manager_id: my_id,
                            id: target_user_id
                        },
                        attributes: ['user_master_id']
                    }).then((user_list) => {
                        if (user_list) {
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    });
                }
            });
    });
}
module.exports = router;
