const express = require("express"),
        router = express.Router();
const mime = require("mime");
const base64 = require("file-base64");
var passport = require('passport');
require('../passport')(passport);
var jwt = require('jsonwebtoken');
const salt = require('../parameters').passwordHash;
const UserModel = require('../models/users');
const UserProffessionalDetailsModel = require("../models/user_proffessional_details");
const UserPersonalDetailsModel = require("../models/user_personal_details");
const JobTitlesModel = require("../models/job_titles");
const Sequelize = require("sequelize");
const sequelize = require('../db');
const Op = Sequelize.Op;

router.post("/directory_shortlist", passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var userfinal = [];
        UserModel.findOne(
                {
                    where: {
                        id: req.body.id
                    }
                }).then((user) => {
            return new Promise(function (resolve, reject) {
                if (user.user_roles_id <= 2) {
                    UserModel.findAll({
                        where: {
                            id: {
                                [Op.not]: req.body.id
                            },
                            [Sequelize.Op.not]: {
                                user_roles_id: 1
                            }
                        },
                        attributes: ['id']
                    }).then((user_list) => {
                        console.log(user_list);

                        
                        user_list.map((elem) => {
                            userfinal.push(elem.id);
                        });
                        resolve();
                    });
                } else {
                    UserProffessionalDetailsModel.findAll({
                        
                        attributes: ['user_master_id']
                    }).then((user_list) => {
                        user_list.map((elem) => {
                            userfinal.push(elem.user_master_id);
                        });
                        resolve();
                    });
                }

            }).then(() => {
                UserModel.findAll({
                    where: {
                        id: {
                            [Op.in]: userfinal,
                            [Op.not]: req.body.id
                        },
                        
                    },
                    required: true,
                    attributes: ['id'],
                    include: [{
                            model: UserPersonalDetailsModel,
                            as: 'PersonalDetail',
                            attributes: ['id', 'image_location', 'first_name_en', 'first_name_th', 'middle_name_en', 'middle_name_th', 'last_name_en', 'last_name_th'],
                            required: true
                        },
                        {
                            model: UserProffessionalDetailsModel,
                            as: 'ProffessionalDetail',
                            attributes: ['id'],
                            required: true,
                            include: [
                                {
                                    model: JobTitlesModel,
                                    as: 'JobTitlesMaster',
                                    attributes: ['name'],
                                    required: true
                                }
                            ]
                        }
                    ],
                    order: [
                        [Sequelize.literal('RAND()')]
                    ],
                    limit: 5
                }).then(users => {
                    res.send({
                        status: res.statusCode,
                        users,
                        success: true
                    });
                });
            });
        });

    }
});
router.post("/user_special_days", passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var month = req.body.month;
        sequelize.query(`SELECT  CASE WHEN MONTH(per.dob) = 10
          THEN 'Birthday' ELSE 'Work Aniversary' 
       END AS type, CASE WHEN MONTH(per.dob) = 10
          THEN per.dob ELSE prof.joining_date
       END AS date,u.id, per.image_location, per.first_name_en, per.first_name_th, per.middle_name_en, 
per.middle_name_th, per.last_name_en, per.last_name_th FROM dbo.user_master u 
INNER JOIN dbo.user_personal_details per on u.id=per.user_master_id 
INNER JOIN dbo.user_proffessional_details prof on u.id=prof.user_master_id WHERE u.id != 
${req.body.id} AND (MONTH(per.dob)=${month} OR MONTH(prof.joining_date)=${month});`, {model: UserModel}).then((user_details) => {
            res.json({code: 200, success: true, data: user_details});
        });
    }
});
router.post("/requests", passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var month = req.body.month;
        sequelize.query(`SELECT CASE WHEN uoa.status=0
            THEN CONCAT(per.first_name_en,' ',per.last_name_en, ' applied for a Overtime'), created_at 
            WHEN uua.status=0
            THEN CONCAT(per.first_name_en,' ',per.last_name_en, ' applied for a Outdoor'), created_at 
            WHEN ura.status=0
            THEN CONCAT(per.first_name_en,' ',per.last_name_en, ' applied for a Reimbursement'), created_at 
            WHEN usa.status=0
            THEN CONCAT(per.first_name_en,' ',per.last_name_en, ' Requested for a Shift Change'), created_at 
            WHEN uwa.status=0
            THEN CONCAT(per.first_name_en,' ',per.last_name_en, ' Requested for Work From Home'), created_at 
             
            END AS message FROM dbo.user_master u LEFT JOIN dbo.user_leave_application ula ON u.id= ula.user_master_id
            INNER JOIN dbo.user_personal_details per ON u.id= per.user_master_id
            INNER JOIN dbo.user_proffessional_details upd ON u.id= upd.user_master_id
            LEFT JOIN dbo.user_overtime_application uoa ON u.id= uoa.user_master_id
            LEFT JOIN dbo.user_outdoor_application uua ON u.id= uua.user_master_id
            LEFT JOIN dbo.user_reimbursement_application ura ON u.id= ura.user_master_id
            LEFT JOIN dbo.user_shift_application usa ON u.id= usa.user_master_id
            LEFT JOIN dbo.user_work_from_home_application uwa ON u.id= uwa.user_master_id
            WHERE u.id !=${req.body.id} AND (
            ula.status=0
            OR uoa.status=0
            OR uua.status=0
            OR ura.status=0
            OR usa.status=0
            OR uwa.status=0 
            ORDER BY 
            uoa.created_at DESC,
            uua.created_at DESC,
            ura.created_at DESC,
            usa.created_at DESC,
            uwa.created_at DESC LIMIT 10 
);`, {model: UserModel}).then((user_details) => {
            res.json({code: 200, success: true, data: user_details});
        });
    }
});
saveFileToDiskAndGetFilename = function (request_file, path) {
    if (request_file !== null) {
        return new Promise(function (resolve, reject) {

            let base64File = request_file.split(';base64,').pop();
            let fileType = request_file.split(';base64,')[0];
            fileType = fileType.substr(5);
            let ext = mime.getExtension(fileType);
            base64.decode(base64File, path + '.' + ext, function (err, output) {
                if (!err) {
                    // console.log(output);
                    resolve(output);
                } else {
                    reject(err);
                }
            });
        })
    }

}

isUserUnderMySupervision = (my_id, target_user_id) => {
    console.log("my_id: ", my_id, " target_user_id: ", target_user_id);
    return new Promise(function (resolve, reject) {
        UserModel.findOne(
                {
                    where: {
                        id: my_id
                    }
                }).then(({user_roles_id}) => {
            if (user_roles_id <= 2) {
                resolve(true);
            } else {
                UserProffessionalDetailsModel.findOne({
                    where: {
                        manager_id: my_id,
                        id: target_user_id
                    },
                    attributes: ['user_master_id']
                }).then((user_list) => {
                    if (user_list) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        }
        });
    });
}
module.exports = router;
