const express = require("express"),
        router = express.Router();
const mime = require("mime");
const base64 = require("file-base64");
var passport = require('passport');
require('../passport')(passport);
var jwt = require('jsonwebtoken');
const salt = require('../parameters').passwordHash;
// 7gdETjnuVA3AW23nwP5k
const TaskModel = require('../models/tasks');
const UserModel = require('../models/users');
const UserProffessionalDetailsModel = require("../models/user_proffessional_details");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

router.post("/add", (req, res, next) => {
    const {user_master_id, task, company_code, extention} = req.body;
    // console.log(task);
    var path = 'public/uploads/task/';
    path = path + company_code + "_" + user_master_id + +new Date();
    let filename = company_code + "_" + user_master_id + +new Date();
    saveFileToDiskAndGetFilename(task, path).then(data => {
        TaskModel.create({
            user_master_id,
            task: `${filename}.${extention}`
        }).then(savedtask => {
            res.json({
                status: res.statusCode,
                savedtask
            });
        });
    });
});
router.post("/get", (req, res, next) => {
    const {user_id, company_code} = req.body
    TaskModel.findAll(
            {
                where: {user_master_id: user_id},
                include: [
                    {
                        model: UserModel,
                        as: 'UserMaster'
                    }
                ]
            }
    ).then(tasks => {
        res.json({
            status: res.statusCode,
            tasks,
            success: true
        });
    });
});
router.post("/request_list", passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var userfinal = [];
        UserModel.findOne(
                {
                    where: {
                        id: req.body.id
                    }
                }).then((user) => {
            return new Promise(function (resolve, reject) {
                if (user.user_roles_id <= 2) {
                    UserModel.findAll({
                        where: {
                            id: {
                                [Op.not]: req.body.id
                            }
                        },
                        attributes: ['id']
                    }).then((user_list) => {
                        user_list.map((elem) => {
                            userfinal.push(elem.id);
                        });
                        resolve();
                    });
                } else {
                    UserProffessionalDetailsModel.findAll({
                        where: {
                            manager_id: req.body.id
                        },
                        attributes: ['user_master_id']
                    }).then((user_list) => {
                        user_list.map((elem) => {
                            userfinal.push(elem.user_master_id);
                        });
                        resolve();
                    });
                }

            }).then(() => {
                TaskModel.findAll(
                        {
                            where: {
                                user_master_id: {
                                    [Op.in]: userfinal
                                }
                            },
                            include: [
                                {
                                    model: UserModel,
                                    as: 'UserMaster',
                                    attributes: {
                                        exclude: ['password']
                                    }
                                }
                            ]
                        }
                ).then(tasks => {
                    res.json({
                        status: res.statusCode,
                        tasks,
                        success: true
                    });
                });
            });
        });
    }
});
router.post("/approve", passport.authenticate('jwt', {session: false}), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        TaskModel.findOne({
            where: {
                id: req.body.task_id
            },
            attributes: ['user_master_id']
        }).then(({user_master_id}) => {
            isUserUnderMySupervision(req.body.id, user_master_id).then((data) => {
                
                if (data) {
                    TaskModel.update(
                            {
                                approval_remarks: req.body.task_remark,
                                approval_date: new Date(Date.now()).toISOString(),
                                status: req.body.status,
                                approver_id: req.body.id
                            },
                            {
                                where: {
                                    id: req.body.task_id,
                                    status: 0
                                }
                            }
                    ).then((updated_data) => {
                        res.json({
                            status: res.statusCode,
                            msg: 'Data Updated Successfully',
                            success: true
                        });
                    });
                }else{
                    res.json({
                            status: res.statusCode,
                            msg: 'Not Allowed to Update',
                            success: true
                        });
                }
            });
        });
    }
});


saveFileToDiskAndGetFilename = function (request_file, path) {
    if (request_file !== null) {
        return new Promise(function (resolve, reject) {

            let base64File = request_file.split(';base64,').pop();
            let fileType = request_file.split(';base64,')[0];
            fileType = fileType.substr(5);
            let ext = mime.getExtension(fileType);
            base64.decode(base64File, path + '.' + ext, function (err, output) {
                if (!err) {
                    // console.log(output);
                    resolve(output);
                } else {
                    reject(err);
                }
            });
        });
    }
}

isUserUnderMySupervision = (my_id, target_user_id) => {
    console.log("my_id: ", my_id, " target_user_id: ", target_user_id);
    return new Promise(function (resolve, reject) {
        UserModel.findOne(
                {
                    where: {
                        id: my_id
                    }
                }).then(({user_roles_id}) => {
            if (user_roles_id <= 2) {
                resolve(true);
            } else {
                UserProffessionalDetailsModel.findOne({
                    where: {
                        manager_id: my_id,
                        id: target_user_id
                    },
                    attributes: ['user_master_id']
                }).then((user_list) => {
                    if (user_list.length) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                });
        }
        });
    });
}
module.exports = router;
