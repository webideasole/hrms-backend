/**
 * Developer - Pathikrit Sanyal
 * @type Module express|Module express
 * This route file uses JWT And passport tokens for user Authentication.
 * For MSSQL ORM usability We are using Sequelize Goodluck reading their docs for debug purpose!!!!
 * Enough Tech talk Happy coding!!!! 
 */
var express = require('express');
var router = express.Router();
var base64 = require('file-base64');
const mime = require("mime");
var base64ToImage = require('base64-to-image');
const Sequelize = require('sequelize');
const sequelize = require('../db');
const nodemailer = require("nodemailer");
var UserModel = require('../models/users');
var CountryModel = require('../models/countries');
var JobTitlesModel = require('../models/job_titles');
var BranchesModel = require('../models/branches');
var DepartmentsModel = require('../models/department');
var SectionsModel = require('../models/sections');
var SubSectionsModel = require('../models/sub_sections');
var LocationsModel = require('../models/locations');
var DivisionsModel = require('../models/divisions');
var LevelsModel = require('../models/levels');
var OccupationGroupsModel = require('../models/occupation_groups');
var BussinessUnitsModel = require('../models/bussiness_units');
var CostCentersModel = require('../models/cost_centers');
var CustomGroupsModel = require('../models/custom_groups');
var PaycodesModel = require('../models/paycodes');
var LeavesModel = require('../models/leaves');
var ShiftsModel = require('../models/shifts');
var SalaryPaymentTypeMasterModel = require('../models/salary_payment_type_master');
var UserPersonalDetailsModel = require('../models/user_personal_details');
var UserProffessionalDetailsModel = require('../models/user_proffessional_details');
var UserInsuranceDetailsModel = require('../models/user_insurance_details');
var UserLeaveAllocationsModel = require('../models/user_leave_allocation');
var UserShiftAllocationsModel = require('../models/user_shift_allocation');
var UserPaycodeAllocationsModel = require('../models/user_paycode_allocation');
var UserSalaryDetailsModel = require('../models/user_salary_details');
var UserLeaveApplicationPermissionModel = require('../models/user_leave_application_permission');
var UserShiftApplicationPermissionModel = require('../models/user_shift_application_permission');
var UserLeaveApplicationModel = require('../models/user_leave_applications');
var UserOvertimeApplicationModel = require('../models/user_overtime_applications');
var UserOutdoorApplicationModel = require('../models/user_outdoor_applications');
var UserWorkFromHomeApplicationModel = require('../models/user_work_from_home_applications');
var UserReimbursementApplicationModel = require('../models/user_reimbursement_applications');
var UserShiftApplicationModel = require('../models/user_shift_applications');
var AdminPermissionDetails = require('../models/admin_permission_details');
var UserResignationApplication = require('../models/user_resignation_application');
var UserLeaveApproverModel = require('../models/user_leave_approvers');

var passport = require('passport');
var bcrypt = require('bcryptjs');

require('../passport')(passport);
var jwt = require('jsonwebtoken');
const salt = require('../parameters').passwordHash;
/* GET users listing. */
router.get('/', function (req, res, next) {
    res.end('<h1><strong>hola amigos</strong></h1>');
});
/*
 *Common get function Apis to use  in the Website
 *return (success:"TRUE", modelDetails) on success
 */
router.get('/get_nationality', function (req, res, next) {
    CountryModel.findAll().then(countries => {
        res.json({ success: true, countries: countries });
    });
});
router.get('/get_job_titles', function (req, res, next) {
    JobTitlesModel.findAll().then(job_titles => {
        res.json({ success: true, job_titles: job_titles });
    });
});
router.get('/get_branches', function (req, res, next) {
    BranchesModel.findAll().then(branches => {
        res.json({ success: true, branches: branches });
    });
});
router.get('/get_departments', function (req, res, next) {
    DepartmentsModel.findAll().then(departments => {
        res.json({ success: true, departments: departments });
    });
});
router.get('/get_sections', function (req, res, next) {
    SectionsModel.findAll().then(sections => {
        res.json({ success: true, sections: sections });
    });
});
router.post('/get_sub_sections', function (req, res, next) {
    SubSectionsModel.findAll({ where: { section_master_id: req.body.section_id } }).then(sub_sections => {
        res.json({ success: true, sub_sections: sub_sections });
    });
});
router.get('/get_locations', function (req, res, next) {
    LocationsModel.findAll().then(locations => {
        res.json({ success: true, locations: locations });
    });
});
router.get('/get_divisions', function (req, res, next) {
    DivisionsModel.findAll().then(divisions => {
        res.json({ success: true, divisions: divisions });
    });
});
router.get('/get_levels', function (req, res, next) {
    LevelsModel.findAll().then(levels => {
        res.json({ success: true, levels: levels });
    });
});
router.get('/get_occupation_groups', function (req, res, next) {
    OccupationGroupsModel.findAll().then(occupation_groups => {
        res.json({ success: true, occupation_groups: occupation_groups });
    });
});
router.get('/get_bussiness_units', function (req, res, next) {
    BussinessUnitsModel.findAll().then(bussiness_units => {
        res.json({ success: true, bussiness_units: bussiness_units });
    });
});
router.get('/get_cost_centers', function (req, res, next) {
    CostCentersModel.findAll().then(cost_centers => {
        res.json({ success: true, cost_centers: cost_centers });
    });
});
router.get('/get_custom_groups', function (req, res, next) {
    CustomGroupsModel.findAll().then(custom_groups => {
        res.json({ success: true, custom_groups: custom_groups });
    });
});
router.get('/get_paycodes', function (req, res, next) {
    PaycodesModel.findAll().then(paycodes => {
        res.json({ success: true, paycodes: paycodes });
    });
});
router.get('/get_leaves', function (req, res, next) {
    LeavesModel.findAll().then(leaves => {
        res.json({ success: true, leaves: leaves });
    });
});
router.get('/get_shifts', function (req, res, next) {
    ShiftsModel.findAll().then(shifts => {
        res.json({ success: true, shifts: shifts });
    });
});
router.get('/get_salary_payment_methods', function (req, res, next) {
    SalaryPaymentTypeMasterModel.findAll().then(payment_methods => {
        res.json({ success: true, payment_methods: payment_methods });
    });
});
router.post('/get_leave_approvers', function (req, res, next) {
    UserLeaveApplicationPermissionModel.findAll({
        where: { user_master_id: req.body.id },
        include: [
            {
                model: UserPersonalDetailsModel,
                as: 'ApproverPersonalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'ApproverProffessionalDetail'
            },
        ]
    }).then((users) => {
        console.log(Array.from(users));
        let approver = [];
        for (var i in users) {
            approver.push([i, users[i]]);
        }
        setTimeout(() => {

            res.json({ success: true, user: users });
        }, 3000);
    });
});
router.post('/get_shift_approvers', function (req, res, next) {
    UserShiftApplicationPermissionModel.findAll({
        where: { user_master_id: req.body.id },
        include: [
            {
                model: UserPersonalDetailsModel,
                as: 'Approver1PersonalDetail'
            },
            {
                model: UserPersonalDetailsModel,
                as: 'Approver2PersonalDetail'
            },
            {
                model: UserPersonalDetailsModel,
                as: 'Approver3PersonalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Approver1ProffessionalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Approver2ProffessionalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Approver3ProffessionalDetail'
            },
            {
                model: UserPersonalDetailsModel,
                as: 'Notifier1PersonalDetail'
            },
            {
                model: UserPersonalDetailsModel,
                as: 'Notifier2PersonalDetail'
            },
            {
                model: UserPersonalDetailsModel,
                as: 'Notifier3PersonalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Notifier1ProffessionalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Notifier2ProffessionalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'Notifier3ProffessionalDetail'
            }
        ]
    }).then((users) => {
        res.json({ success: true, user: users });
    });
});
/**
 * Login api to take a user input as username and password and return Authentication result based on that user
 * return Json{status:"ERROR", errorCode, errorMessage} On error,
 * return Json{status:success:true, JwtToken, userDetails} On error,
 * 
 */
router.post('/login', function (req, res, next) {
    if (!('username' in req.body) || !('password' in req.body)) {
        res.json({ "status": "ERROR", "code": "999", "msg": "Please enter all the Details" });
        res.end();
    } else {
        let username = req.body.username;
        let password = req.body.password;
        let company_code = req.body.company_code;
        UserModel.findOne({
            where: { username: username, company_code: company_code, status: 1 }, include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'PersonalDetail'
                }]
        }).then(user => {
            if (user) {
                user.validPassword(password).then((check) => {
                    if (check) {
                        var token = jwt.sign(user.toJSON(), salt);
                        res.json({ success: true, token: 'JWT ' + token, user: user });
                    } else {
                        res.json({ "status": "ERROR", "code": "998", "msg": "Password Does Not Match" });
                        res.end();
                    }
                })

            } else {
                res.json({ "status": "ERROR", "code": "997", "msg": "Username Not Found" });
                res.end();
            }

        });
    }

});
/**
 * create employees this section is available to HR and CFHR only
 */
router.post('/create_employee', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    // router.post('/create_employee', function (req, res, next) {
    // console.log(req.body);

    var token = getToken(req.headers);
    if (token) {
        var base64Str = req.body.personal.image_name;
        var path = 'public/uploads/profile_images/';
        var optionalObj = { 'fileName': req.body.personal.employee_code };
        var imageInfo = base64ToImage(base64Str, path, optionalObj);
        req.body.personal.image_location = imageInfo.fileName;
        req.body.allocation[1].created_at = new Date(Date.now()).toISOString();
        req.body.allocation[1].status = 1;
        UserModel.find({
            where: {
                username: req.body.personal.employee_code,
                company_code: req.body.company_code,
            }
        }).then(user => {
            // console.log('\x1b[33m%s\x1b[0m', user);

            if (user) {
                res.json({
                    success: false,
                    masg: "User Already Exists"
                })
            } else {
                newLeaveAllocation = [];
                console.log(req.body.allocation[2]);

                // req.body.allocation[2].async.each(async (res) => {
                req.body.allocation[2].forEach((res) => {
                    LeavesModel.find({
                        where: {
                            id: res.leave_master_id,
                        }
                    }).then(data => {
                        // console.log(data.count);

                        let obj = {
                            leave_master_id: res.leave_master_id,
                            carry_forward: 0,
                            entitlement: 0,
                            ytd_availed: 0,
                            balance: data.count
                        };
                        newLeaveAllocation.push(obj)
                    })
                })
                setTimeout(() => {
                    console.log("Bla Bla", newLeaveAllocation);

                    UserModel.create(
                        {
                            "user_roles_id": req.body.proffesional.user_type,
                            "company_code": req.body.company_code,
                            "username": req.body.personal.employee_code,
                            "password": req.body.personal.employee_code,
                            "status": 1,
                            "created_at": new Date(Date.now()).toISOString(),
                            "PersonalDetail": req.body.personal,
                            "ProffessionalDetail": req.body.proffesional,
                            "UserLeaveApplicationPermission": req.body.leave_permission_array,
                            "UserShiftApplicationPermission": req.body.shift_permission,
                            "InsuranceDetail": req.body.insurance,
                            "SalaryDetail": req.body.salary,
                            "LeaveAllocations": newLeaveAllocation,
                            "ShiftAllocations": req.body.allocation[1],
                            "PaycodeAllocations": req.body.allocation[0]
                        },
                        {
                            include: [
                                {
                                    model: UserPersonalDetailsModel,
                                    as: 'PersonalDetail'
                                },
                                {
                                    model: UserProffessionalDetailsModel,
                                    as: 'ProffessionalDetail'
                                },
                                {
                                    model: UserInsuranceDetailsModel,
                                    as: 'InsuranceDetail'
                                },
                                {
                                    model: UserLeaveApplicationPermissionModel,
                                    as: 'UserLeaveApplicationPermission'
                                },
                                {
                                    model: UserShiftApplicationPermissionModel,
                                    as: 'UserShiftApplicationPermission'
                                },
                                {
                                    model: UserSalaryDetailsModel,
                                    as: 'SalaryDetail'
                                },
                                {
                                    model: UserLeaveAllocationsModel,
                                    as: 'LeaveAllocations'
                                },
                                {
                                    model: UserShiftAllocationsModel,
                                    as: 'ShiftAllocations'
                                },
                                {
                                    model: UserPaycodeAllocationsModel,
                                    as: 'PaycodeAllocations'
                                }
                            ]
                        }
                    ).then((newUser) => {
                        //            console.log(re)

                        res.json({ success: true, msg: "User Has been created Successfully" });
                    });

                }, 5000);

            }
        }, err => {
            console.log(err);

        })

    } else {
        res.json({
            success: false,
            masg: "Token Not Match"
        })
    }
});
/**
 * given a user id and a company code the employee details will be returned
 */
router.post('/view_profile', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserModel.find({
            where: { id: req.body.id, company_code: req.body.company_code },
            include: [{
                model: UserPersonalDetailsModel,
                as: 'PersonalDetail',
                include: [
                    {
                        model: CountryModel,
                        as: 'countryMaster'
                    }
                ]
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'ProffessionalDetail',
                include: [
                    {
                        model: JobTitlesModel,
                        as: 'JobTitlesMaster'
                    },
                    {
                        model: BranchesModel,
                        as: 'BranchesMaster'
                    },
                    {
                        model: DepartmentsModel,
                        as: 'DepartmentsMaster'
                    },
                    {
                        model: SectionsModel,
                        as: 'SectionsMaster'
                    },
                    {
                        model: SubSectionsModel,
                        as: 'SubSectionsMaster'
                    },
                    {
                        model: LocationsModel,
                        as: 'LocationsMaster'
                    },
                    {
                        model: DivisionsModel,
                        as: 'DivisionsMaster'
                    },
                    {
                        model: LevelsModel,
                        as: 'LevelsMaster'
                    },
                    {
                        model: OccupationGroupsModel,
                        as: 'OccupationGroupsMaster'
                    },
                    {
                        model: BussinessUnitsModel,
                        as: 'BussinessUnitsMaster'
                    },
                    {
                        model: CostCentersModel,
                        as: 'CostCentersMaster'
                    },
                    {
                        model: CustomGroupsModel,
                        as: 'CustomGroupsMaster'
                    }
                ]
            },
            {
                model: UserInsuranceDetailsModel,
                as: 'InsuranceDetail'
            },
            {
                model: UserSalaryDetailsModel,
                as: 'SalaryDetail'
            },
            {
                model: UserLeaveAllocationsModel,
                as: 'LeaveAllocations',
                include: [
                    {
                        model: LeavesModel,
                        as: 'LeavesMaster'
                    }
                ]
            },
            {
                model: UserShiftAllocationsModel,
                as: 'ShiftAllocations',
                include: [
                    {
                        model: ShiftsModel,
                        as: 'ShiftsMaster'
                    }
                ]
            },
            {
                model: UserPaycodeAllocationsModel,
                as: 'PaycodeAllocations',
                include: [
                    {
                        model: PaycodesModel,
                        as: 'PaycodesMaster'
                    }
                ]
            }, {
                model: UserLeaveApplicationPermissionModel,
                as: 'UserLeaveApplicationPermission'
            }, {
                model: UserShiftApplicationPermissionModel,
                as: 'UserShiftApplicationPermission'
            }

            ]
        }).then((user) => {
            res.json({ success: true, user: user });
        });
    }
});
/**
 * list of all users
 * 
 */
// router.post('/list_users', passport.authenticate('jwt', {session: false}), function (req, res, next) {
router.post('/list_users', function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserModel.findAll({
            where: {
                company_code: req.body.company_code,
                [Sequelize.Op.not]: {
                    user_roles_id: 1
                }
            },
            include: [{
                model: UserPersonalDetailsModel,
                as: 'PersonalDetail'
            },
            {
                model: UserProffessionalDetailsModel,
                as: 'ProffessionalDetail',
                include: [
                    {
                        model: JobTitlesModel,
                        as: 'JobTitlesMaster'
                    },
                    {
                        model: LocationsModel,
                        as: 'LocationsMaster'
                    }
                ]
            }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((users) => {
            res.json({ success: true, user: users });
        });
    }
});
/**
 * UPDATE employees this section is available to HR and CFHR only
 */
router.post('/update_employee', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        if (req.body.personal.image_name) {
            var base64Str = req.body.personal.image_name;
            var path = 'public/uploads/profile_images/';
            var optionalObj = { 'fileName': req.body.personal.employee_code };
            var imageInfo = base64ToImage(base64Str, path, optionalObj);
            req.body.personal.image_location = imageInfo.fileName;
        }
        UserModel.update(
            {
                "user_roles_id": 3,
                "company_code": req.body.company_code,
                "username": req.body.personal.employee_code,
                "password": req.body.personal.employee_code,
                "status": 1,
                "updated_at": new Date(Date.now()).toISOString()
            },
            {
                where: {
                    id: req.body.id
                }

            }
        ).then((updatedUser) => {
            if (updatedUser) {
                UserModel.findOne({
                    where: { id: req.body.id },
                    include: [{
                        model: UserPersonalDetailsModel,
                        as: 'PersonalDetail'
                    },
                    {
                        model: UserProffessionalDetailsModel,
                        as: 'ProffessionalDetail'
                    },
                    {
                        model: UserInsuranceDetailsModel,
                        as: 'InsuranceDetail'
                    },
                    {
                        model: UserLeaveApplicationPermissionModel,
                        as: 'UserLeaveApplicationPermission'
                    },
                    {
                        model: UserShiftApplicationPermissionModel,
                        as: 'UserShiftApplicationPermission'
                    },
                    {
                        model: UserSalaryDetailsModel,
                        as: 'SalaryDetail'
                    },
                    {
                        model: UserLeaveAllocationsModel,
                        as: 'LeaveAllocations'
                    },
                    {
                        model: UserShiftAllocationsModel,
                        as: 'ShiftAllocations'
                    },
                    {
                        model: UserPaycodeAllocationsModel,
                        as: 'PaycodeAllocations'
                    },

                    ]
                }).then((user) => {
                    let userLeaveIds = user.LeaveAllocations.map(leave => leave.id);
                    let userShiftIds = user.ShiftAllocations.map(shift => shift.id);
                    let userPaycodeIds = user.PaycodeAllocations.map(paycode => paycode.id);
                    req.body.personal.user_master_id = req.body.id;
                    req.body.proffesional.user_master_id = req.body.id;
                    req.body.insurance.user_master_id = req.body.id;
                    req.body.salary.user_master_id = req.body.id;
                    UserPersonalDetailsModel.update(req.body.personal, { where: { id: user.PersonalDetail.id } }).then(() => {
                        UserProffessionalDetailsModel.update(req.body.proffesional, { where: { id: user.ProffessionalDetail.id } }).then(() => {
                            UserInsuranceDetailsModel.update(req.body.insurance, { where: { id: user.InsuranceDetail.id } }).then(() => {
                                UserSalaryDetailsModel.update(req.body.salary, { where: { id: user.SalaryDetail.id } }).then(() => {
                                    UserLeaveApplicationPermissionModel.update(req.body.leave_permission, { where: { id: user.UserLeaveApplicationPermission.id } }).then(() => {
                                        UserShiftApplicationPermissionModel.update(req.body.shift_permission, { where: { id: user.UserShiftApplicationPermission.id } });
                                    });
                                });
                            });
                        });
                    }).then(() => {
                        let userLeaveIds = user.LeaveAllocations.map(leave => leave.id);
                        let userShiftIds = user.ShiftAllocations.map(shift => shift.id);
                        let userPaycodeIds = user.PaycodeAllocations.map(paycode => paycode.id);
                        UserLeaveAllocationsModel.destroy({
                            where: {
                                id: {
                                    [Sequelize.Op.or]: userLeaveIds
                                }
                            }
                        }).then(() => {
                            UserShiftAllocationsModel.destroy({
                                where: {
                                    id: {
                                        [Sequelize.Op.or]: userShiftIds
                                    }
                                }
                            });
                        }).then(() => {
                            UserPaycodeAllocationsModel.destroy({
                                where: {
                                    id: {
                                        [Sequelize.Op.or]: userPaycodeIds
                                    }
                                }
                            });
                        }).then(() => {
                            for (let [key, value] of req.body.allocation.entries()) {
                                for (let [key2, value2] of value.entries()) {
                                    req.body.allocation[key][key2].user_master_id = req.body.id;
                                }
                            }
                            UserLeaveAllocationsModel.bulkCreate(req.body.allocation[2]).then(() => {
                                UserShiftAllocationsModel.bulkCreate(req.body.allocation[1]);
                            }).then(() => {
                                UserPaycodeAllocationsModel.bulkCreate(req.body.allocation[0]);
                            });
                        });
                    });
                });
            }
        }).then(() => {
            res.json({ "success": true });
        });
    }
});
/**
 * employee will apply for leaves from this section and initially it will be in pending state. 
 */
router.post('/create_employee_leave', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var leave_id = req.body.leave_type;
        // console.log(req.body);
        UserLeaveAllocationsModel.findOne({ where: { id: leave_id } }).then((user_leave_details) => {
            // console.log(user_leave_details);

            if (user_leave_details) {
                if (user_leave_details.user_master_id == user_id) {
                    if (user_leave_details.balance > 0.00 && user_leave_details.balance >= req.body.leave_count) {
                        var path = 'public/uploads/leave_applications/';
                        path = path + req.body.company_code + '_' + user_id + (+new Date);

                        if (req.body.leave_attachment != "") {

                            saveFileToDiskAndGetFilename(req.body.leave_attachment, path).then(url => {


                                url = url.split("/");
                                console.log(url);

                                UserLeaveApplicationModel.create(
                                    {
                                        user_master_id: user_id,
                                        user_leave_allocation_id: req.body.leave_type,
                                        substitute_id: req.body.leave_substitute,
                                        attachment: url[url.length - 1],
                                        from: req.body.from,
                                        to: req.body.to,
                                        leave_unit_count: req.body.leave_count,
                                        reason: req.body.leave_remarks,
                                        created_at: new Date(Date.now()).toISOString(),
                                        status: 0, //status is pending
                                        step: 1 //step is pending for 1st approver
                                    }
                                ).then((user_leave_created) => {

                                    res.json({ success: true, msg: 'Leave Application Registered for approval successfully' });
                                });
                            })
                        } else {
                            UserLeaveApplicationModel.create(
                                {
                                    user_master_id: user_id,
                                    user_leave_allocation_id: req.body.leave_type,
                                    substitute_id: req.body.leave_substitute,
                                    from: req.body.from,
                                    to: req.body.to,
                                    leave_unit_count: req.body.leave_count,
                                    reason: req.body.leave_remarks,
                                    created_at: new Date(Date.now()).toISOString(),
                                    status: 0, //status is pending
                                    step: 1 //step is pending for 1st approver
                                }
                            ).then((user_leave_created) => {
                                res.json({ success: true, msg: 'Leave Application Registered for approval successfully' });
                            });
                        }
                    } else {
                        res.json({ code: 98, msg: 'No Leave Available.' })
                    }
                } else {
                    res.json({ code: 99, msg: 'Wrong input' });
                }
            }
        });
    }
});
/**
 * List of employees own leave applications will be shown here.
 */
router.post('/list_my_leave', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserLeaveApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'SubstitutePersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserLeaveAllocationsModel,
                    as: 'LeaveAllocation',
                    attributes: ['leave_master_id'],
                    include: [
                        {
                            model: LeavesModel,
                            as: 'LeavesMaster',
                            attributes: ['name']
                        }
                    ]
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_leaves) => {
            res.json(user_leaves);
        });
    }
});
/**
 * view each leave details 
 */
router.post('/view_leave_details', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserLeaveAllocationsModel.findAll({
            where: { user_master_id: req.body.id },
            include: [
                {
                    model: LeavesModel,
                    as: 'LeavesMaster'
                }
            ]
        }).then((user_leaves) => {
            res.json({ success: true, leave: user_leaves });
        });
    }
});
router.post('/list_leave_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        sequelize.query(`Select ula.id from user_leave_application_permission ulap INNER JOIN user_leave_application ula ON ulap.user_master_id=ula.user_master_id WHERE ulap.approver_id=${approver_id} AND ula.step IS NOT NULL AND ula.step >= ulap.approver_level
        `, { model: UserLeaveApplicationModel }).then((leave_details) => {

            var finalArray = leave_details.map(function (obj) {
                return obj.id;
            });

            UserLeaveApplicationModel.findAll({
                where: {
                    id: {
                        [Sequelize.Op.in]: finalArray
                    }
                },
                include: [
                    {
                        model: UserPersonalDetailsModel,
                        as: 'ApplicantPersonalDetails',
                        attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                    },
                    {
                        model: UserPersonalDetailsModel,
                        as: 'ApproverPersonalDetails',
                        attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                    },
                    {
                        model: UserPersonalDetailsModel,
                        as: 'SubstitutePersonalDetails',
                        attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                    },
                    {
                        model: UserLeaveApplicationPermissionModel,
                        as: 'UserLeaveApplicationPermission',
                        where: {
                            approver_id: approver_id
                        }
                    },
                    {
                        model: UserLeaveAllocationsModel,
                        as: 'LeaveAllocation',
                        attributes: ['leave_master_id'],
                        include: [
                            {
                                model: LeavesModel,
                                as: 'LeavesMaster',
                                attributes: ['name']
                            }
                        ]
                    },
                ],
                order: [
                    ['id', 'DESC']
                ]
            }).then((user_leaves) => {
                if (Array.isArray(user_leaves) && user_leaves.length > 0) {
                    user_leaves.forEach((elem, index) => {
                        console.log(user_leaves[index]);

                        if (elem.status == 1) {
                            user_leaves[index].statusText = "Approved"
                        }
                        else if (elem.status == 0) {
                            user_leaves[index].statusText = "Pending"
                        }
                        else if (elem.status == 3) {
                            user_leaves[index].statusText = "Withdrawn"
                        } else if (elem.status == 2) {
                            user_leaves[index].statusText = "Rejected"
                        } else {
                            var highestLevel = 0;
                            if (elem.UserLeaveApplicationPermission && elem.UserLeaveApplicationPermission.length > 1) {
                                elem.UserLeaveApplicationPermission.forEach((elem2) => {
                                    if (highestLevel == 0) {
                                        highestLevel = elem2.approver_level
                                    } else if (highestLevel < elem2.approver_level) {
                                        highestLevel = elem2.approver_level
                                    }
                                })
                            } else {
                                highestLevel = elem.UserLeaveApplicationPermission[0].approver_level
                            }
                            if (highestLevel && parseInt(elem.step) == highestLevel) {
                                user_leaves[index].statusText = "Pending"
                            } else if (highestLevel && parseInt(elem.step) > highestLevel) {
                                user_leaves[index].statusText = "Approved"
                            } else {
                                user_leaves[index].statusText = "Error"
                            }
                        }
                        console.log(user_leaves)
                    })
                }
                res.json(user_leaves);
            });
        });
    }
});


/**
 * User  will withdraw their leaves from here. 
 */
router.post('/withdraw_my_leave', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserLeaveApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_leave_created) => {
            if (user_leave_created) {
                if (user_leave_created[0] == 1) {
                    res.json({ success: true, msg: 'Leave Application Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Leave.' });
                }
            }
        });
    }
});
/**
 * Approver will Approve leaves from this section reject/ approve. 
 */
router.post('/approve_employee_leave', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var leave_id = req.body.leave_id;
        var leave_allocation_id = req.body.leave_allocation_id;
        UserLeaveApplicationModel.findOne(
            {
                where: {
                    id: leave_id
                }
            }).then((user_leave_data_before_update) => {
                if (user_leave_data_before_update) {

                    UserLeaveAllocationsModel.findOne({ where: { id: leave_allocation_id } }).then((user_leave_details) => {
                        if (user_leave_details) {
                            if (user_leave_details.balance >= user_leave_data_before_update.leave_unit_count) {
                                parseInt(user_leave_data_before_update.step)
                                UserLeaveApplicationPermissionModel.findAll({
                                    limit: 1,
                                    order: [
                                        // Will escape title and validate DESC against a list of valid direction parameters
                                        ['approver_level', 'DESC']
                                    ]                             
                                }).then(data => {
                                    if (data.length > 0) {
                                        if(data[0].approver_id == user_id) {
                                            UserLeaveApplicationModel.update(
                                                {
                                                    approval_remarks: req.body.leave_remarks,
                                                    approval_date: new Date(Date.now()).toISOString(),
                                                    status: 1,
                                                    step: parseInt(user_leave_data_before_update.step) + 1
                                                },
                                                {
                                                    where: {
                                                        id: leave_id,
                                                        // status: 0
                                                    }
                                                }
                                            ).then((user_leave_created) => {
                                                if (user_leave_created[0] == 1) {
                                                    if (req.body.status == 1) {
                                                        UserLeaveAllocationsModel.update(
                                                            {
                                                                balance: parseFloat(user_leave_details.balance) - user_leave_data_before_update.leave_unit_count,
                                                                ytd_availed: parseFloat(user_leave_details.ytd_availed) + user_leave_data_before_update.leave_unit_count
                                                            },
                                                            {
                                                                where: {
                                                                    id: user_leave_data_before_update.user_leave_allocation_id
                                                                }
                                                            }
                                                        ).then((updated_allocation) => {
                                                            if (updated_allocation[0] == 1) {
                                                                UserLeaveApproverModel.create({
                                                                    leave_application_id: user_leave_data_before_update.id,
                                                                    approver_id: user_id,
                                                                    approver_level: user_leave_data_before_update.step,
                                                                    created_at: new Date(Date.now()).toISOString(),
                                                                    approval_status: req.body.status //status is pending
                                                                }
                                                                ).then((user_leave_created) => {

                                                                    res.json({ success: true, msg: 'Leave Application approved successfully' });
                                                                });
                                                            } else {
                                                                res.json({ code: 99, msg: 'You are not allowed to update this Leave right Now.' });
                                                            }
                                                        });
                                                    } else {
                                                        res.json({ success: true, msg: 'Leave Application Rejected successfully' });
                                                    }
                                                } else {
                                                    res.json({ code: 98, msg: 'You are not allowed to update this Leave right Now.' });
                                                }
                                            });
                                        }else{
                                            UserLeaveApplicationModel.update(
                                                {
                                                    approval_remarks: req.body.leave_remarks,
                                                    approval_date: new Date(Date.now()).toISOString(),
                                                    status: req.body.status == 2 ? req.body.status : parseInt(user_leave_data_before_update.step) + 3,//status is pending
                                                    step: parseInt(user_leave_data_before_update.step) + 1
                                                },
                                                {
                                                    where: {
                                                        id: leave_id,
                                                        // status: 0
                                                    }
                                                }
                                            ).then((user_leave_created) => {
                                                if (user_leave_created[0] == 1) {
                                                    if (req.body.status == 1) {
                                                        UserLeaveAllocationsModel.update(
                                                            {
                                                                balance: parseFloat(user_leave_details.balance) - user_leave_data_before_update.leave_unit_count,
                                                                ytd_availed: parseFloat(user_leave_details.ytd_availed) + user_leave_data_before_update.leave_unit_count
                                                            },
                                                            {
                                                                where: {
                                                                    id: user_leave_data_before_update.user_leave_allocation_id
                                                                }
                                                            }
                                                        ).then((updated_allocation) => {
                                                            if (updated_allocation[0] == 1) {
                                                                UserLeaveApproverModel.create({
                                                                    leave_application_id: user_leave_data_before_update.id,
                                                                    approver_id: user_id,
                                                                    approver_level: user_leave_data_before_update.step,
                                                                    created_at: new Date(Date.now()).toISOString(),
                                                                    approval_status: req.body.status //status is pending
                                                                }
                                                                ).then((user_leave_created) => {

                                                                    res.json({ success: true, msg: 'Leave Application approved successfully' });
                                                                });
                                                            } else {
                                                                res.json({ code: 99, msg: 'You are not allowed to update this Leave right Now.' });
                                                            }
                                                        });
                                                    } else {
                                                        res.json({ success: true, msg: 'Leave Application Rejected successfully' });
                                                    }
                                                } else {
                                                    res.json({ code: 98, msg: 'You are not allowed to update this Leave right Now.' });
                                                }
                                            });
                                        }
                                    }                                                                        
                                })
                                
                            } else {
                                res.json({ code: 97, msg: 'No Leave Available.' });
                            }
                        } else {
                            res.json({ code: 96, msg: 'Cannot Change Leave Status.' });
                        }
                    });
                } else {
                    res.json({ code: 95, msg: 'Cannot Change Leave Status.' });
                }
            });
    }
});
/**
 * employee will apply for Overtimes from this section and initially it will be in pending state. 
 */
router.post('/employee_overtime_apply', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var path = 'public/uploads/overtime_applications/';
        path = path + req.body.company_code + '_' + user_id + (+new Date);
        if (req.body.overtime_attachment != "") {

            saveFileToDiskAndGetFilename(req.body.overtime_attachment, path).then(url => {
                // console.log(url);

                url = url.split('/')
                UserOvertimeApplicationModel.create(
                    {
                        user_master_id: user_id,
                        date: req.body.overtime_date,
                        from_time: req.body.overtime_from_time,
                        reason: req.body.overtime_remarks,
                        to_time: req.body.overtime_to_time,
                        attachment: url[url.length - 1],
                        created_at: new Date(Date.now()).toISOString(),
                        status: 0, //status is pending
                        step: 1
                    }
                ).then((user_leave_created) => {
                    res.json({ success: true, msg: 'OverTime Application Registered for approval successfully' });
                });
            })
        } else {
            UserOvertimeApplicationModel.create(
                {
                    user_master_id: user_id,
                    approver_id: req.body.approver,
                    date: req.body.overtime_date,
                    from_time: req.body.overtime_from_time,
                    reason: req.body.overtime_remarks,
                    to_time: req.body.overtime_to_time,
                    attachment: null,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0, //status is pending
                    step: 1
                }
            ).then((user_leave_created) => {
                res.json({ success: true, msg: 'OverTime Application Registered for approval successfully' });
            });
        }
    }
});
/**
 * List of employees own overtime applications will be shown here.
 */
router.post('/list_my_overtimes', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserOvertimeApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_overtime) => {
            res.json(user_overtime);
        });
    }
});
/**
 * User  will withdraw their OVERTIMES from here. 
 */
router.post('/withdraw_my_overtime', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserOvertimeApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_overtime_withdrawn) => {
            if (user_overtime_withdrawn) {
                if (user_overtime_withdrawn[0] == 1) {
                    res.json({ success: true, msg: 'Overtime Application Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Leave.' });
                }
            }
        });
    }
});
/**
 * Approver will Approve overtime from this section reject/ approve. 
 */
router.post('/approve_employee_overtime', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var overtime_id = req.body.overtime_id;
        UserOvertimeApplicationModel.update(
            {
                approval_remarks: req.body.overtime_remarks,
                approval_date: new Date(Date.now()).toISOString(),
                status: req.body.status == 2 ? req.body.status : parseInt(user_leave_data_before_update.step) + 3,//status is pending
                step: parseInt(user_leave_data_before_update.step) + 1
            },
            {
                where: {
                    id: overtime_id,
                    // approver_id: user_id,
                    // status: 0
                }
            }
        ).then((user_overtime_approved) => {
            if (user_overtime_approved[0] == 1) {
                UserLeaveApproverModel.create({
                    leave_application_id: user_leave_data_before_update.id,
                    approver_id: user_id,
                    approver_level: user_leave_data_before_update.step,
                    created_at: new Date(Date.now()).toISOString(),
                    approval_status: req.body.status //status is pending
                }
                ).then((user_leave_created) => {
                    if (req.body.status == 1) {
                        res.json({ success: true, msg: 'Overtime Application Approved successfully' });
                    } else if (req.body.status == 2) {
                        res.json({ success: true, msg: 'Overtime Application Rejected successfully' });
                    }
                });
            } else {
                res.json({ code: 98, msg: 'You are not allowed to update this overtime right Now.' });
            }
        });
    }
});
/**
 * List of Suborldinate employees overtime applications will be shown here to the Approver.
 */
router.post('/list_overtime_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        sequelize.query(`Select uoa.id from user_leave_application_permission ulap INNER JOIN user_overtime_application uoa ON ulap.user_master_id=uoa.user_master_id WHERE ulap.approver_id=${approver_id} AND uoa.step IS NOT NULL AND uoa.step >= ulap.approver_level
        `, { model: UserOvertimeApplicationModel }).then((leave_details) => {

            var finalArray = leave_details.map(function (obj) {
                return obj.id;
            });

            UserLeaveApplicationModel.findAll({
                where: {
                    id: {
                        [Sequelize.Op.in]: finalArray
                    }
                },
                include: [
                    {
                        model: UserPersonalDetailsModel,
                        as: 'ApplicantPersonalDetails',
                        attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                    },
                    {
                        model: UserPersonalDetailsModel,
                        as: 'ApproverPersonalDetails',
                        attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                    },
                    {
                        model: UserLeaveApplicationPermissionModel,
                        as: 'UserLeaveApplicationPermission',
                        where: {
                            approver_id: approver_id
                        }
                    },
                ],
                order: [
                    ['id', 'DESC']
                ]
            }).then((user_overtimes) => {
                if (Array.isArray(user_overtimes) && user_overtimes.length > 0) {
                    user_overtimes.forEach((elem, index) => {
                        console.log(user_overtimes[index]);

                        if (elem.status == 1) {
                            user_overtimes[index].statusText = "Approved"
                        }
                        else if (elem.status == 0) {
                            user_overtimes[index].statusText = "Pending"
                        }
                        else if (elem.status == 3) {
                            user_overtimes[index].statusText = "Withdrawn"
                        } else if (elem.status == 2) {
                            user_overtimes[index].statusText = "Rejected"
                        } else {
                            var highestLevel = 0;
                            if (elem.UserLeaveApplicationPermission && elem.UserLeaveApplicationPermission.length > 1) {
                                elem.UserLeaveApplicationPermission.forEach((elem2) => {
                                    if (highestLevel == 0) {
                                        highestLevel = elem2.approver_level
                                    } else if (highestLevel < elem2.approver_level) {
                                        highestLevel = elem2.approver_level
                                    }
                                })
                            } else {
                                highestLevel = elem.UserLeaveApplicationPermission[0].approver_level
                            }
                            if (highestLevel && parseInt(elem.step) == highestLevel) {
                                user_overtimes[index].statusText = "Pending"
                            } else if (highestLevel && parseInt(elem.step) > highestLevel) {
                                user_overtimes[index].statusText = "Approved"
                            } else {
                                user_overtimes[index].statusText = "Error"
                            }
                        }
                        console.log(user_overtimes)
                    })
                }
                res.json(user_overtimes);
            });
        });
    }
});
/**
 * employee will apply for outdoor from this section and initially it will be in pending state. 
 */
router.post('/employee_outdoor_apply', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.user_master_id;
        var path = 'public/uploads/outdoor_applications/';
        path = path + req.body.company_code + '_' + user_id + (+new Date);
        if (req.body.outdoor_attachment != "") {

            saveFileToDiskAndGetFilename(req.body.outdoor_attachment, path).then(url => {
                url = url.split("/");
                UserOutdoorApplicationModel.create(
                    {
                        user_master_id: user_id,
                        approver_id: req.body.approver,
                        date_count: req.body.outdoor_count,
                        start_date: req.body.outdoor_from_date,
                        end_date: req.body.outdoor_to_date,
                        start_time: req.body.outdoor_from_time,
                        reason: req.body.outdoor_remarks,
                        end_time: req.body.outdoor_to_time,
                        attachment: url[url.length - 1],
                        created_at: new Date(Date.now()).toISOString(),
                        status: 0 //status is pending
                    }
                ).then((user_outdoor_created) => {
                    res.json({ success: true, msg: 'Outdoor Application Registered for approval successfully' });
                });
            })
        } else {
            UserOutdoorApplicationModel.create(
                {
                    user_master_id: user_id,
                    approver_id: req.body.approver,
                    date_count: req.body.outdoor_count,
                    start_date: req.body.outdoor_from_date,
                    end_date: req.body.outdoor_to_date,
                    start_time: req.body.outdoor_from_time,
                    reason: req.body.outdoor_remarks,
                    end_time: req.body.outdoor_to_time,
                    attachment: null,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0 //status is pending
                }
            ).then((user_outdoor_created) => {
                res.json({ success: true, msg: 'Outdoor Application Registered for approval successfully' });
            });
        }
    }
});
/**
 * User  will withdraw their OVERTIMES from here. 
 */
router.post('/withdraw_my_outdoor', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserOutdoorApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_outdoor_withdrawn) => {
            if (user_outdoor_withdrawn) {
                if (user_outdoor_withdrawn[0] == 1) {
                    res.json({ success: true, msg: 'Outdoor Application Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Leave.' });
                }
            }
        });
    }
});
/**
 * List of employees own outdoor applications will be shown here.
 */
router.post('/list_my_outdoors', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserOutdoorApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_outdoor) => {
            console.log(user_outdoor);
            res.json(user_outdoor);
        });
    }
});
/**
 * Approver will Approve outdoor from this section reject/ approve. 
 */
router.post('/approve_employee_outdoor', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var outdoor_id = req.body.outdoor_id;
        UserOutdoorApplicationModel.update(
            {
                approval_remarks: req.body.outdoor_remarks,
                approval_date: new Date(Date.now()).toISOString(),
                status: req.body.status//status is pending
            },
            {
                where: {
                    id: outdoor_id,
                    approver_id: user_id,
                    status: 0
                }
            }
        ).then((user_outdoor_approved) => {
            if (user_outdoor_approved[0] == 1) {
                if (req.body.status == 1) {
                    res.json({ success: true, msg: 'Outdoor Application Approved successfully' });
                } else if (req.body.status == 2) {
                    res.json({ success: true, msg: 'Outdoor Application Rejected successfully' });
                }
            } else {
                res.json({ code: 98, msg: 'You are not allowed to update this overtime right Now.' });
            }
        });
    }
});
/**
 * List of Suborldinate employees outdoor applications will be shown here to the Approver.
 */
router.post('/list_outdoors_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        UserOutdoorApplicationModel.findAll({
            where: {
                approver_id: approver_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_outdoor) => {
            res.json(user_outdoor);
        });
    }
});
/**
 * employee will apply for wORK fROM hOME from this section and initially it will be in pending state. 
 */
router.post('/employee_work_from_home_apply', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.user_master_id;
        var path = 'public/uploads/wfh_attachment/';
        path = path + req.body.company_code + '_' + user_id + (+new Date);
        if (req.body.wfh_attachment != "") {

            saveFileToDiskAndGetFilename(req.body.wfh_attachment, path).then(url => {
                url = url.split("/")
                UserWorkFromHomeApplicationModel.create(
                    {
                        user_master_id: user_id,
                        approver_id: req.body.approver,
                        date_count: req.body.wfh_count,
                        start_date: req.body.wfh_from_date,
                        end_date: req.body.wfh_to_date,
                        start_time: req.body.wfh_from_time,
                        reason: req.body.wfh_remarks,
                        end_time: req.body.wfh_to_time,
                        attachment: url[url.length - 1],
                        created_at: new Date(Date.now()).toISOString(),
                        status: 0 //status is pending
                    }
                ).then((user_work_from_home_created) => {
                    res.json({ success: true, msg: 'Work From Home Application Registered for approval successfully' });
                });
            })
        } else {
            UserWorkFromHomeApplicationModel.create(
                {
                    user_master_id: user_id,
                    approver_id: req.body.approver,
                    date_count: req.body.wfh_count,
                    start_date: req.body.wfh_from_date,
                    end_date: req.body.wfh_to_date,
                    start_time: req.body.wfh_from_time,
                    reason: req.body.wfh_remarks,
                    end_time: req.body.wfh_to_time,
                    attachment: null,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0 //status is pending
                }
            ).then((user_work_from_home_created) => {
                res.json({ success: true, msg: 'Work From Home Application Registered for approval successfully' });
            });
        }
    }
});
/**
 * User  will withdraw their Work from home from here. 
 */
router.post('/withdraw_my_work_from_home', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserWorkFromHomeApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_work_from_home_withdrawn) => {
            if (user_work_from_home_withdrawn) {
                if (user_work_from_home_withdrawn[0] == 1) {
                    res.json({ success: true, msg: 'Work From Home Application Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Application.' });
                }
            }
        });
    }
});
/**
 * List of employees own Work From Home applications will be shown here.
 */
router.post('/list_my_work_from_homes', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserWorkFromHomeApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_work_from_home) => {
            res.json(user_work_from_home);
        });
    }
});
/**
 * Approver will Approve Work From Home from this section reject/ approve. 
 */
router.post('/approve_employee_work_from_home', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var wfh_id = req.body.wfh_id;
        UserWorkFromHomeApplicationModel.update(
            {
                approval_remarks: req.body.wfh_remarks,
                approval_date: new Date(Date.now()).toISOString(),
                status: req.body.status//status is pending
            },
            {
                where: {
                    id: wfh_id,
                    approver_id: user_id,
                    status: 0
                }
            }
        ).then((user_wfh_approved) => {
            if (user_wfh_approved[0] == 1) {
                if (req.body.status == 1) {
                    res.json({ success: true, msg: 'Work From Home Application Approved successfully' });
                } else if (req.body.status == 2) {
                    res.json({ success: true, msg: 'Work From Home Application Rejected successfully' });
                }
            } else {
                res.json({ code: 98, msg: 'You are not allowed to update this overtime right Now.' });
            }
        });
    }
});
/**
 * List of Suborldinate employees Work from Home applications will be shown here to the Approver.
 */
router.post('/list_work_from_homes_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        UserWorkFromHomeApplicationModel.findAll({
            where: {
                approver_id: approver_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_wfh) => {
            res.json(user_wfh);
        });
    }
});
/**
 * employee will apply for Reimbursement from this section and initially it will be in pending state. 
 */
router.post('/employee_reimbursement_apply', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.user_master_id;
        var path = 'public/uploads/reimbursement_attachment/';
        path = path + req.body.company_code + '_' + user_id + (+new Date);
        if (req.body.reim_attachment != "") {

            saveFileToDiskAndGetFilename(req.body.reim_attachment, path).then(url => {
                // req.body.reim_attachment
                url = url.split("/")
                UserReimbursementApplicationModel.create(
                    {
                        user_master_id: user_id,
                        approver_id: req.body.approver,
                        claim_type: req.body.reim_claim_type,
                        start_date: req.body.reim_from_date,
                        end_date: req.body.reim_to_date,
                        date_count: req.body.reim_count,
                        start_time: req.body.reim_from_time,
                        end_time: req.body.reim_to_time,
                        amount: req.body.reim_amount,
                        reason: req.body.reim_remarks,
                        attachment: url[url.length - 1],
                        created_at: new Date(Date.now()).toISOString(),
                        status: 0 //status is pending
                    }
                ).then((user_reimbursement_created) => {
                    res.json({ success: true, msg: 'Reimbursement Application Registered for approval successfully' });
                });
            })
        } else {
            UserReimbursementApplicationModel.create(
                {
                    user_master_id: user_id,
                    approver_id: req.body.approver,
                    claim_type: req.body.reim_claim_type,
                    start_date: req.body.reim_from_date,
                    end_date: req.body.reim_to_date,
                    date_count: req.body.reim_count,
                    start_time: req.body.reim_from_time,
                    end_time: req.body.reim_to_time,
                    amount: req.body.reim_amount,
                    reason: req.body.reim_remarks,
                    attachment: null,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0 //status is pending
                }
            ).then((user_reimbursement_created) => {
                res.json({ success: true, msg: 'Reimbursement Application Registered for approval successfully' });
            });
        }
    }
});
/**
 * User  will withdraw their Reimbursement from here. 
 */
router.post('/withdraw_my_reimbursement', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserReimbursementApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_reimbursement_withdrawn) => {
            if (user_reimbursement_withdrawn) {
                if (user_reimbursement_withdrawn[0] == 1) {
                    res.json({ success: true, msg: 'Reimbursement Application Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Application.' });
                }
            }
        });
    }
});
/**
 * List of employees own Reimbursement applications will be shown here.
 */
router.post('/list_my_reimbursements', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserReimbursementApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_reimbursement) => {
            res.json(user_reimbursement);
        });
    }
});
/**
 * Approver will Approve Reimbursement from this section reject/ approve. 
 */
router.post('/approve_employee_reimbursement', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var reim_id = req.body.reim_id;
        UserReimbursementApplicationModel.update(
            {
                approval_remarks: req.body.reim_remarks,
                approval_date: new Date(Date.now()).toISOString(),
                status: req.body.status//status is pending
            },
            {
                where: {
                    id: reim_id,
                    approver_id: user_id,
                    status: 0
                }
            }
        ).then((user_reimbursement_approved) => {
            if (user_reimbursement_approved[0] == 1) {
                if (req.body.status == 1) {
                    res.json({ success: true, msg: 'Reimbursement Application Approved successfully' });
                } else if (req.body.status == 2) {
                    res.json({ success: true, msg: 'Reimbursement Application Rejected successfully' });
                }
            } else {
                res.json({ code: 98, msg: 'You are not allowed to update this Reimbursemnt right Now.' });
            }
        });
    }
});
/**
 * List of Suborldinate employees Reimbursement applications will be shown here to the Approver.
 */
router.post('/list_reimbursements_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        UserReimbursementApplicationModel.findAll({
            where: {
                approver_id: approver_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_wfh) => {
            res.json(user_wfh);
        });
    }
});
/**
 * employee will apply for Shift from this section and initially it will be in pending state. 
 */
router.post('/employee_shift_apply', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.user_master_id;
        var path = 'public/uploads/shift_attachment/';
        path = path + req.body.company_code + '_' + user_id + (+new Date);
        // let filename = company_code + "_" + user_master_id + +new Date();
        if (req.body.shift_attachment) {

            saveFileToDiskAndGetFilename(req.body.shift_attachment, path).then(url => {
                // req.body.shift_attachment 
                url = url.split('/');
                UserShiftApplicationModel.create(
                    {
                        user_master_id: user_id,
                        approver_id: req.body.approver,
                        start_date: req.body.shift_from_date,
                        end_date: req.body.shift_to_date,
                        date_count: req.body.shift_count,
                        start_time: req.body.shift_from_time,
                        end_time: req.body.shift_to_time,
                        old_shift_id: req.body.shift_old,
                        new_shift_id: req.body.shift_new,
                        reason: req.body.shift_remarks,
                        attachment: url[url.length - 1],
                        created_at: new Date(Date.now()).toISOString(),
                        status: 0 //status is pending
                    }
                ).then((user_shift_created) => {
                    res.json({ success: true, msg: 'Shift Application Registered for approval successfully' });
                });
            })
        } else {
            UserShiftApplicationModel.create(
                {
                    user_master_id: user_id,
                    approver_id: req.body.approver,
                    start_date: req.body.shift_from_date,
                    end_date: req.body.shift_to_date,
                    date_count: req.body.shift_count,
                    start_time: req.body.shift_from_time,
                    end_time: req.body.shift_to_time,
                    old_shift_id: req.body.shift_old,
                    new_shift_id: req.body.shift_new,
                    reason: req.body.shift_remarks,
                    attachment: null,
                    created_at: new Date(Date.now()).toISOString(),
                    status: 0 //status is pending
                }
            ).then((user_shift_created) => {
                res.json({ success: true, msg: 'Shift Application Registered for approval successfully' });
            });
        }
    }
});
/**
 * returns the current users shift from user_shift_allocation table
 */
router.post('/know_my_current_shift', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        let user_id = req.body.id;
        sequelize
            .query(
                `SELECT Top 1 [ShiftAllocations].[id], [ShiftsMaster].[id] AS [shift_id], [ShiftsMaster].[name] AS [shift_name]
 FROM [user_shift_allocation] AS [ShiftAllocations] LEFT OUTER JOIN [shift_master] AS [ShiftsMaster] ON [ShiftAllocations].[shift_master_id] = 
 [ShiftsMaster].[id] WHERE [ShiftAllocations].[user_master_id] = ${user_id} AND [ShiftAllocations].[end_date] IS NULL ORDER BY [ShiftAllocations].[id] 
 DESC;`,
                { model: UserShiftAllocationsModel }
            )
            .then(user_current_shift => {
                // console.log("current shift :...", user_current_shift);
                res.json({
                    code: 200,
                    success: true,
                    msg: user_current_shift
                });
            });
    }
});
/**
 * User  will withdraw their Shift Changes from here. 
 */
router.post('/withdraw_my_shift_change', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var my_id = req.body.id;
        var application_id = req.body.application_id;
        UserShiftApplicationModel.update(
            {
                status: 3//status is WITHDRAWN
            },
            {
                where: {
                    id: application_id,
                    user_master_id: my_id,
                    status: 0
                }

            }
        ).then((user_shift_change_withdrawn) => {
            if (user_shift_change_withdrawn) {
                if (user_shift_change_withdrawn[0] == 1) {
                    res.json({ success: true, msg: 'Application For Shift Change Cancelled successfully' });
                } else {
                    res.json({ code: 99, msg: 'You are Not Allowed to WithDraw this Application.' });
                }
            }
        });
    }
});
/**
 * List of employees own Shift Changes applications will be shown here.
 */
router.post('/list_my_shift_changes', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        user_id = req.body.id;
        UserShiftApplicationModel.findAll({
            where: {
                user_master_id: user_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: ShiftsModel,
                    as: 'OldShiftDetails',
                    attributes: ['id', 'name']
                },
                {
                    model: ShiftsModel,
                    as: 'NewShiftDetails',
                    attributes: ['id', 'name']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((user_shift_appilication) => {
            res.json(user_shift_appilication);
        });
    }
});
/**
 * Approver will Approve Shift Change from this section reject/ approve. 
 */
router.post('/approve_employee_shift_change', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        var user_id = req.body.id;
        var shift_id = req.body.shift_id;
        UserShiftApplicationModel.update(
            {
                approval_remarks: req.body.shift_remarks,
                approval_date: new Date(Date.now()).toISOString(),
                status: req.body.status//status is pending
            },
            {
                where: {
                    id: shift_id,
                    approver_id: user_id,
                    status: 0
                }
            }
        ).then((user_shift_change_approved) => {
            if (user_shift_change_approved[0] == 1) {
                if (req.body.status == 1) {
                    res.json({ success: true, msg: 'Shift Change Application Approved successfully' });
                } else if (req.body.status == 2) {
                    res.json({ success: true, msg: 'Shift Change Application Rejected successfully' });
                }
            } else {
                res.json({ code: 98, msg: 'You are not allowed to update this Shift Change right Now.' });
            }
        });
    }
});
/**
 * List of Suborldinate employees Shift Change applications will be shown here to the Approver.
 */
router.post('/list_shift_change_for_approvers', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        approver_id = req.body.id;
        UserShiftApplicationModel.findAll({
            where: {
                approver_id: approver_id
            },
            include: [
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApplicantPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: UserPersonalDetailsModel,
                    as: 'ApproverPersonalDetails',
                    attributes: ['prefix', 'first_name_en', 'middle_name_en', 'last_name_en']
                },
                {
                    model: ShiftsModel,
                    as: 'OldShiftDetails',
                    attributes: ['id', 'name']
                },
                {
                    model: ShiftsModel,
                    as: 'NewShiftDetails',
                    attributes: ['id', 'name']
                }
            ],
            order: [
                ['id', 'DESC']
            ]
        }).then((employee_shift_change) => {
            res.json(employee_shift_change);
        });
    }
});
/**
 * List of Suborldinate employees Shift Change applications will be shown here to the Approver.
 */
router.post('/get_my_employee_ids', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    var token = getToken(req.headers);
    if (token) {
        UserModel.findOne(
            {
                where: {
                    id: req.body.id
                }
            }).then((user) => {
                if (user.user_roles_id <= 2) {
                    UserModel.findAll({
                        attributes: ['id']
                    }).then((user_list) => {
                        let userfinal = []
                        user_list.map((elem) => {
                            userfinal.push(elem.id);
                        });
                        res.json({ success: true, data: userfinal });
                    });
                } else {
                    UserProffessionalDetailsModel.findAll({
                        where: {
                            manager_id: req.body.id
                        },
                        attributes: ['user_master_id']
                    }).then((user_list) => {
                        let userfinal = []
                        user_list.map((elem) => {
                            userfinal.push(elem.user_master_id);
                        });
                        res.json({ success: true, data: userfinal });
                    });
                }
            });
    }
});

router.post('/create_admin', passport.authenticate('jwt', { session: false }), function (req, res, next) {

    const { email, username, company_code, password, payroll, leave, time } = req.body;
    UserModel.find({
        where: {
            username: username,
            company_code: company_code,
        }
    }).then(user => {
        if (user) {
            res.json({
                success: false,
                masg: "User Already Exists"
            })
        } else {

            UserModel.create({
                "username": username,
                "company_code": company_code,
                "password": password,
                "email": email,
                "user_roles_id": 1,
                "status": 1,
                "created_at": new Date(Date.now()).toISOString(),
            }).then(userdata => {
                if (userdata) {
                    AdminPermissionDetails.create({
                        user_master_id: userdata.id,
                        payroll, leave, time
                    }).then(permissiondata => {
                        res.json({
                            success: true,
                            permissiondata
                        })
                    })
                }
            })
        }

    })
})

router.post('/list_admins', passport.authenticate('jwt', { session: false }), function (req, res, next) {

    const { company_code } = req.body;
    UserModel.findAll({
        where: {
            "user_roles_id": 1,
            "company_code": company_code
        },
        attributes: { exclude: ['password'] },
        include: [
            {
                model: AdminPermissionDetails,
                as: 'AdminPermissionDetail',
            },
        ]

    }).then(users => {
        if (users) {
            res.json({
                success: true,
                users
            })
        }
    })

})

router.post('/deactive', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    const { id } = req.body;
    sequelize
        .query(`UPDATE user_master
                SET status = CASE WHEN status = 1 THEN 0 
                WHEN status = 0 THEN 1
                ELSE status END WHERE id = ${id};`, { model: UserModel }).then(data => {
            if (data) {
                res.json({
                    success: true,
                    data
                })
            }
        })
})

router.post('/change-pass', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    const { id, old_pass, new_pass } = req.body;

    UserModel.find({
        where: {
            "id": id,
            // "status" : 1
        }
    }).then(user => {
        console.log(user);

        if (user) {
            bcrypt.compare(old_pass, user.password, function (err, result) {
                console.log(result);

                let newBcryptPass = bcrypt.hashSync(new_pass, salt);
                if (result) {
                    UserModel.update({
                        "password": newBcryptPass
                    }, {
                            where: {
                                "id": id
                            }
                        }).then(updated => {
                            res.json({
                                success: true,
                                updated
                            })
                        })
                } else {
                    res.json({
                        success: false,
                        msg: "Password Not Match."
                    })
                }
            })

        } else {
            res.json({
                success: false,
                msg: "No Such User Found"
            })
        }
    })
});

router.post('/create_resignation', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    const {
        user_master_id,
        reason,
        date,
        notice_period,
        last_working_date,
        attachment,
    } = req.body;
    var path = 'public/uploads/resignation/';
    path = path + req.body.company_code + '_' + user_master_id + (+new Date);
    // let filename = company_code + "_" + user_master_id + +new Date();
    if (attachment != "") {

        saveFileToDiskAndGetFilename(attachment, path).then(url => {
            let newdate = new Date(date);
            let newworkingdate = new Date(last_working_date);

            console.log(url);

            url = url.split('/');
            UserResignationApplication.create({
                user_master_id,
                reason,
                date: newdate.toISOString(),
                notice_period,
                last_working_day: newworkingdate.toISOString(),
                attachment: url[url.length - 1],
                created_at: new Date(Date.now()).toISOString(),
                updated_at: new Date(Date.now()).toISOString(),
            }).then(resignation => {
                if (resignation) {
                    res.json({
                        success: true,
                        resignation
                    })
                }
            })
        })
    } else {
        UserResignationApplication.create({
            user_master_id,
            reason,
            date: newdate.toISOString(),
            notice_period,
            last_working_day: newworkingdate.toISOString(),
            attachment: null,
            created_at: new Date(Date.now()).toISOString(),
            updated_at: new Date(Date.now()).toISOString(),
        }).then(resignation => {
            if (resignation) {
                res.json({
                    success: true,
                    resignation
                })
            }
        })
    }

})

router.post('/update_resignation', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    const {
        id,
        approver_id,
        remarks,
        approval_date,
    } = req.body;
    let new_approval_date = new Date(approval_date)
    UserResignationApplication.update({
        approver_id,
        remarks,
        approval_date: new_approval_date.toISOString(),
        updated_at: new Date(Date.now()).toISOString(),
    },
        {
            where: {
                id
            }
        }).then(updated => {
            if (updated) {
                res.json({
                    status: true,
                    updated
                })
            }
        })


})



/**
 * 
 * @param {type} headers
 * @returns {nm$_users.getToken.parted}
 */
getToken = function (headers) {
    if (headers && headers.authorization) {
        var parted = headers.authorization.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

saveFileToDiskAndGetFilename = function (request_file, path) {
    // console.warn("Herererer", request_file);
    if (request_file) {
        return new Promise(function (resolve, reject) {

            let base64File = request_file.split(';base64,').pop();
            let fileType = request_file.split(';base64,')[0];
            fileType = fileType.substr(5);
            let ext = mime.getExtension(fileType);
            base64.decode(base64File, path + '.' + ext, function (err, output) {
                if (!err) {
                    console.log(output);
                    resolve(output);
                } else {
                    reject(err);
                }
            });
        });
    }
};

const asyncHandler = fn => (req, res, next) =>
    Promise
        .resolve(fn(req, res, next))
        .catch(next)
module.exports = router;
